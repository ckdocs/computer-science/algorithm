# Asymptotic Analysis

Analysing an algorithm in three cases:

1. **Best Case**: Cost of `best inputs` of size **n**
2. **Worst Case**: Cost of `worst inputs` of size **n**
3. **Average Case**: Probabilistic `average` cost of `all inputs` of size **n**

---

## Asymptotic Notations

To express the `boundries of costs` in **Best, Worst, Average Cases**, we use **Asymptotic Notations** or **Growth Of Functions Notations**

These notations are **Mathematical Sets** containing **Functions** with same growth:

-   **Big O**: $O$
-   **Little O**: $o$
-   **Big Omega**: $\Omega$
-   **Little Omega**: $\omega$
-   **Theta**: $\Theta$

![Asymptotic Notations](../assets/asymptotic_notations.png)

---

### Comparing

There are two ways for comparing growth of functions:

-   **Rules**: We can use `rules` of famous functions

    $$
    \begin{aligned}
        & 1 \ll \log{n} \ll n \ll n\log{n} = \log{(n!)} \ll n^2 \ll (\log{n})! \ll 2^n \ll n! \ll n^n
    \end{aligned}
    $$

    ![Comparing Rules](../assets/asymptotic_notations_rules.gif)

-   **Visualize**: We can `visualize` that functions

    -   **Example**: Is this expression true? `Yes`

    $$
    \begin{aligned}
        & f(n) = \Omega(n) \;\And\; g(n) = O(n^2) \implies \frac{g(n)}{f(n)} = O(n)
    \end{aligned}
    $$

    ![Comparing Visualize](../assets/asymptotic_notations_visualize.png)

---

### Concepts

#### Non Comparable

There are exists some function that are not $O$ or $\Omega$ of each other:

$$
f \;?\; g \implies
\begin{cases}
    f \in O(g): 3n^2 + 2n \in O(n^3)
    \\
    f \in \Omega(g): 3n^2 + 2n \in \Omega(2n)
    \\
    f \in O(g) \land f \in \Omega(g): 3n^2 + 2n \in \Theta(n^2)
    \\
    f \not\in O(g) \land f \not\in \Omega(g): n \not\in O(n^{1+\sin{n}}) \land \Omega(n^{1+\sin{n}})
\end{cases}
$$

> -   $f = g$
> -   $f \ll g$
> -   $f \gg g$
> -   $f \neq g$
>
> ![Order Of Growth](../assets/order-of-growth.png)
>
> ![Unknown](../assets/unknown.png)

---

#### Reflexive Relation

$$
\begin{aligned}
    & f \in O(f)
    \\
    & f \in \Omega(f)
    \\
    & f \in \Theta(f)
\end{aligned}
$$

---

#### Symmetric Relation

$$
\begin{aligned}
    & f \in \Theta(g) \iff g \in \Theta(f)
\end{aligned}
$$

---

#### Skew-Symmetric Relation

$$
\begin{aligned}
    & f \in O(g) \iff g \in \Omega(f)
    \\
    & f \in o(g) \iff g \in \omega(f)
\end{aligned}
$$

---

#### Transitive Relation

$$
\begin{aligned}
    & f \in O(g) \land g \in O(h) \implies f \in O(h)
    \\
    & f \in \Omega(g) \land g \in \Omega(h) \implies f \in \Omega(h)
    \\
    & f \in o(g) \land g \land o(h) \implies f \in o(h)
    \\
    & f \in \omega(g) \land g \in \omega(h) \implies f \in \omega(h)
    \\
    & f \in \Theta(g) \land g \in \Theta(h) \implies f \in \Theta(h)
\end{aligned}
$$

---

#### Maximum Property

$$
\begin{aligned}
    & f + g \in O(Max\{f, g\})
    \\
    & f + g \in \Omega(Max\{f, g\})
    \\
    & f + g \in \Theta(Max\{f, g\})
    \\
    \\
    & \texttt{If K was constant:}
    \\
    & f_1 + f_2 + \dots + f_k \in O(Max\{f_1, f_2, \dots, f_k\})
    \\
    & f_1 + f_2 + \dots + f_k \in \Omega(Max\{f_1, f_2, \dots, f_k\})
    \\
    & f_1 + f_2 + \dots + f_k \in \Theta(Max\{f_1, f_2, \dots, f_k\})
\end{aligned}
$$

> **Example**

$$
\begin{aligned}
    & n^2 + 3n \in \Theta(n^2)
    \\
    & (n + n^2).2^n \in \Theta(n^2.2^n)
\end{aligned}
$$

---

#### Exponent Property

$$
\begin{aligned}
    & f(n) \in O(g(n)) \nRightarrow 2^{f(n)} \in O(2^{g(n)})
    \\
    & f(n) \in \Omega(g(n)) \nRightarrow 2^{f(n)} \in \Omega(2^{g(n)})
    \\
    & f(n) \in \Theta(g(n)) \nRightarrow 2^{f(n)} \in \Theta(2^{g(n)})
\end{aligned}
$$

---

### Big O (O)

This **Set** contains functions that:

$$
\begin{aligned}
    & f(n) \in O(g(n)) \iff \exists\; c \geq 0 \;;\; f(n) \ll c.g(n)
\end{aligned}
$$

![Big O](../assets/big_o_notation.jpg)

---

-   **Example**: What is the order of $3n^2 + 2n + 6$ in Big O notation?

$$
\begin{aligned}
    & 3n^2 + 2n + 6 \in O(n^2)
    \\ \iff
    &
        \exists\; n_0 \;,\;
        \exists\; c \geq 0 \;;\;
        \forall\; n \geq n_0 \;:\;
        3n^2 + 2n + 6 \leq c.n^2
    \\ \implies
    & c = 4
\end{aligned}
$$

---

-   **Example**: What is the members of $O(n^2)$ ?

Is a `set` contains `infinite number` of `functions` with `growth` less than or equal to $n^2$:

$$
\begin{aligned}
    & O(n^2) = \{\dots, \lg{n}, n, n^2, 2n^2, \frac{1}{2}n^2, \dots\}
\end{aligned}
$$

---

### Little O (o)

This **Set** contains functions that:

$$
\begin{aligned}
    & f(n) \in o(g(n)) \iff \forall\; c \geq 0 \;,\; f(n) \ll c.g(n)
\end{aligned}
$$

---

-   **Tip**

$$
\begin{aligned}
    & f(n) \in o(g(n)) \implies f(n) \in O(g(n))
    \\
    & o(f) \implies O(f)
    \\
    & \lt \implies \leq
\end{aligned}
$$

---

-   **Tip**: There exists some functions that $f \in O$ and $f \not\in \Omega$ but also $f \not\in o$

$$
\begin{aligned}
    & o(f) \subseteq O(f) - \Omega(f)
    \\
    & \lt \;\subseteq\; \leq - \geq
\end{aligned}
$$

-   **Example**:

$$
\begin{aligned}
    & f(n) =
    \begin{cases}
        n & \texttt{n is even}
        \\
        n^2 & \texttt{n is odd}
    \end{cases}
    \\
    \\
    & f(n) \in O(n^2)
    \\
    & f(n) \not\in \Omega(n^2)
    \\
    & f(n) \not\in o(n^2)
\end{aligned}
$$

---

-   **Example**: What is the order of $3n^2 + 2n + 6$ in Little O notation?

$$
\begin{aligned}
    & 3n^2 + 2n + 6 \not\in o(n^2)
    \\ \implies
    &
        \exists\; n_0 \;;\;
        \forall\; c \geq 0 \;,\;
        \forall\; n \geq n_0 \;:\;
        3n^2 + 2n + 6 \lt c.n^2
    \\ \implies
    & c = 2
    \\
    \\
    & 3n^2 + 2n + 6 \in o(n^3)
    \\ \implies
    &
        \exists\; n_0 \;;\;
        \forall\; c \geq 0 \;,\;
        \forall\; n \geq n_0 \;:\;
        3n^2 + 2n + 6 \lt c.n^3
\end{aligned}
$$

---

### Big Omega (Ω)

This **Set** contains functions that:

$$
\begin{aligned}
    & f(n) \in \Omega(g(n)) \iff \exists\; c \geq 0 \;;\; f(n) \gg c.g(n)
\end{aligned}
$$

![Big Omega](../assets/omega_notation.jpg)

---

-   **Example**: What is the order of $3n^2 + 2n + 6$ in Big Omega notation?

$$
\begin{aligned}
    & 3n^2 + 2n + 6 \in \Omega(n^2)
    \\ \iff
    &
        \exists\; n_0 \;,\;
        \exists\; c \geq 0 \;;\;
        \forall\; n \geq n_0 \;:\;
        3n^2 + 2n + 6 \geq c.n^2
    \\ \implies
    & c = 2
\end{aligned}
$$

---

-   **Example**: What is the members of $\Omega(n^2)$ ?

Is a `set` contains `infinite number` of `functions` with `growth` greater than or equal to $n^2$:

$$
\begin{aligned}
    & \Omega(n^2) = {\dots, n^2, 2^n, 2n^2, n^3, \dots}
\end{aligned}
$$

---

### Little Omega (ω)

This **Set** contains functions that:

$$
\begin{aligned}
    & f(n) \in \omega(g(n)) \iff \forall\; c \geq 0 \;,\; f(n) \gg c.g(n)
\end{aligned}
$$

---

-   **Tip**

$$
\begin{aligned}
    & f(n) \in \omega(g(n)) \implies f(n) \in \Omega(g(n))
    \\
    & \omega(f) \implies \Omega(f)
    \\
    & \gt \implies \geq
\end{aligned}
$$

---

-   **Tip**: There exists some functions that $f \in \Omega$ and $f \not\in O$ but also $f \not\in \omega$

$$
\begin{aligned}
    & \omega(f) \subseteq \Omega(f) - O(f)
    \\
    & \gt \;\subseteq\; \geq - \leq
\end{aligned}
$$

-   **Example**:

$$
\begin{aligned}
    & f(n) =
    \begin{cases}
        n & \texttt{n is even}
        \\
        n^2 & \texttt{n is odd}
    \end{cases}
    \\
    \\
    & f(n) \in \Omega(n)
    \\
    & f(n) \not\in O(n)
    \\
    & f(n) \not\in \omega(n)
\end{aligned}
$$

---

-   **Example**: What is the order of $3n^2 + 2n + 6$ in Little Omega notation?

$$
\begin{aligned}
    & 3n^2 + 2n + 6 \not\in \omega(n^2)
    \\ \implies
    &
        \exists\; n_0 \;;\;
        \forall\; c \geq 0 \;,\;
        \forall\; n \geq n_0 \;:\;
        3n^2 + 2n + 6 \gt c.n^2
    \\ \implies
    & c = 4
    \\
    \\
    & 3n^2 + 2n + 6 \in \omega(n)
    \\ \implies
    &
        \exists\; n_0 \;;\;
        \forall\; c \geq 0 \;,\;
        \forall\; n \geq n_0 \;:\;
        3n^2 + 2n + 6 \gt c.n
\end{aligned}
$$

---

### Theta (θ)

This **Set** contains functions that:

$$
\begin{aligned}
    & f(n) \in \Theta(g(n)) \iff \exists\; c_1 \geq 0 \;;\; \exists\; c_2 \geq 0 \;;\; c1.g(n) \ll f(n) \ll c2.g(n)
\end{aligned}
$$

![Theta](../assets/theta_notation.jpg)

---

-   **Tip**

$$
\begin{aligned}
    & O(f) \;\cap\; \Omega(f) = \Theta(f)
    \\
    & \leq \land \geq \implies =
    \\
    \\
    & o(f) \;\cap\; \omega(f) = \emptyset
    \\
    & \lt \land \gt \implies \emptyset
\end{aligned}
$$

---

-   **Tip**

$$
\begin{aligned}
    & \Theta(f) \implies O(f) \lor \Omega(f)
    \\
    & = \implies \leq \lor \geq
\end{aligned}
$$

---

-   **Example**: What is the order of $3n^2 + 2n + 6$ in Theta notation?

$$
\begin{aligned}
    & 3n^2 + 2n + 6 \in \Theta(n^2)
    \\ \iff
    &
        \exists\; n_0 \;,\;
        \exists\; c_1 \geq 0 \;;\;
        \exists\; c_2 \geq 0 \;;\;
        \forall\; n \geq n_0 \;:\;
        c_1.n^2 \leq 3n^2 + 2n + 6 \leq c_2.n^2
    \\ \implies
    & c_1 = 1, c_2 = 4
\end{aligned}
$$

---

## Best Case

The minimum complexity per input of size **n**

$$
\begin{aligned}
    Min(T(n)) = T(n_i) \,;\, n_i \in n: \texttt{Best}
\end{aligned}
$$

-   **Example**: The best cost for **HashMap** operation: $O(1)$
-   **Example**: The best input for **Linear Search** is that target is in first index

$$
\begin{aligned}
    & [1,0,0,\dots,0]: find(1) = O(1)
\end{aligned}
$$

---

## Worst Case

The maximum complexity per input of size **n**

$$
\begin{aligned}
    Max(T(n)) = T(n_i) \,;\, n_i \in n: \texttt{Worst}
\end{aligned}
$$

-   **Example**: The worst cost for **HashMap** operation: $O(n)$
-   **Example**: The worst input for **Linear Search** is that target is in last index

$$
\begin{aligned}
    & [0,0,0,\dots,1]: find(1) = O(n)
\end{aligned}
$$

---

## Average Case

The average complexity per any input of size **n**

Find expectation with probability of occurance of each input

$$
\begin{aligned}
    Avg(T(n)) = \sum_{n_i \in n} P(n_i) \times T(n_i)
\end{aligned}
$$

-   **Example**: The average cost for **HashMap Successful** operation: $O(1 + \alpha)$
-   **Example**: The average cost for **HashMap Unsuccessful** operation: $O(\frac{1}{1 - \alpha})$
-   **Example**: The average cost for **Linear Search** is the expectation

$$
\begin{aligned}
    & [1,0,0,\dots,0]: find(1) = O(1) \implies P = \frac{1}{n}
    \\
    & [0,1,0,\dots,0]: find(1) = O(2) \implies P = \frac{1}{n}
    \\
    & \vdots
    \\
    & [0,0,0,\dots,1]: find(1) = O(n) \implies P = \frac{1}{n}
    \\
    \\ \implies
    & E(find) = \frac{1}{n}.O(1) + \frac{1}{n}.O(2) + \dots + \frac{1}{n}.O(n)
    \\
    & E(find) = O(\frac{1 + 2 + \dots + n}{n})
    \\
    & E(find) = O(\frac{\frac{n.(n+1)}{2}}{n}) = O(\frac{n+1}{2})
\end{aligned}
$$

-   **Example**: With probability of $\frac{1}{2}$, **x** exists in array, what is the average cost of search x in array of size **n**

$$
\begin{aligned}
    & E(search(x, arr)) = \frac{1}{2}.n + \frac{1}{2}.(\frac{1}{n}.1 + \frac{1}{n}.2 + \dots + \frac{1}{n}.n)
    \\
    \\ \implies
    & E(search(x, arr)) = \frac{3n + 1}{4}
\end{aligned}
$$

---
