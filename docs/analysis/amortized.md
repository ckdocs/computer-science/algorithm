# Amortized Analysis

Sometimes we want to find the **Average Complexity** when calling a function **n Times**, there are three methods to find the amortized cost:

1. **Aggregate Method**
2. **Accounting Method**
3. **Potential Method**

---

## Aggregate Method

What is the cost of **n** times **push** in **Dynamic Array**

1. Most of the time, pushing is **O(1)**
2. Some times, array is full then **O(n)**

Balance the **Worst Cases** to previous steps

$$
\begin{aligned}
    & \texttt{Amortized Cost} = \frac{Cost(\texttt{n Operations})}{n}
\end{aligned}
$$

---

-   **Example**: What is the amortized cost of adding **n** elements to **Dynamic Array**:

$$
\begin{aligned}
    & add(1): 1
    \\
    & add(2): 2
    \\
    & add(3): 1
    \\
    & add(4): 4
    \\
    & add(5): 1
    \\
    \\ \implies
    & addN(x): 1 + 2 + 1 + 4 + 1 + 1 + 1 + 8 + \dots
    \\
    \\
    & Amortized(addN(x)) = \frac{1 + 2 + 1 + 4 + 1 + 1 + 1 + 8 + \dots}{n}
\end{aligned}
$$

---

## Accounting Method

Known as **Banker Method**, in this method we add a **Cost** or **Credit** to each **Element** for next large operation

Then average summation of these tokens

---

## Potential Method

Instead of storing the **Extra Cost** as credit, the potential method represents the extra cost as potential energy or just potential.

---
