# Analysis

Analysing an algorithm consists of two main operations:

1. **Proof** the correction of algorithm
2. Find the **Complexity** of algorithm

In tis tutorial, we mostly focus on **Priori Analysis** or **Algorithm Complexity**

The complexity of algorithm **f(n)** expressed by **Time Complexity** and **Space Complexity** which depends on **Input Size**

---

## Tip (Limited Inputs)

An algorithm will **Halt** and has no **Side-Effects**, so if an algorithm has `limited` inputs it will halt after a `Constant Time` using `Constant Space` with order of `O(1)`

---

-   **Example**: We have an algorithm gets a **8x8 Chess Table State** and tells the **White wins or not**:

![Chess State](../assets/chess_state.webp)

Input is any states of a **8x8** chess table, each cell has **13 states** so all the possible inputs count is:

$$
\begin{aligned}
    & 13^{8*8} = 13^{64}
\end{aligned}
$$

This is constant, so we can solve by some **if** conditions in **O(1)** complexity

---

## Proof

Proof that the algorithm will return **Correct Output** per **Any Input**

---

## Complexity

Suppose **X** is an `algorithm` and **n** is the **size of input data**, the **time** and **space** used by the algorithm X are the two `main factors`, which decide the **efficiency** of X:

1. **Time Factor** ($T(n)$): Number of key operations such as comparisons
2. **Space Factor** ($S(n)$): Maximum memory space required

$$
\begin{aligned}
    & T: n \mapsto \texttt{Number of operations}
    \\
    & S: n \mapsto \texttt{Needed space}
\end{aligned}
$$

---

### Time

It’s a function describing the amount of `time` required to run an algorithm in terms of the **size of the input**

-   **Time**: number of operations (instructions)

---

### Space

It’s a function describing the amount of `memory` an algorithm takes in terms of the **size of input** to the algorithm

-   **Memory**: extra needed memory of algorithm

---

## Analysis

Is the process of finding **Algorithm Complexity** in situations

There are two types of algorithm analysis:

1. **Priori Analysis**: Before implementation (Time, Space complexity)
2. **Posterior Analysis**: After implementation (Debugger tools)

---

### Priori Analysis (Mathematical)

Independent of params, we count the number of **Operations** and **Memory Allocations** to estimate the **Time** and **Space** required by the program

Analysing algorithms, is a function of **Input Size**, so it has a finite range of **Inputs**, one input may be better for algorithm an another may be difficult to solve

There are two main ways of analysing algorithms:

1. **Asymptotic Analysis**: Analysing for `Best`, `Worst`, `Average` inputs
2. **Amortized Analysis**: Analysing by `Repeating` an `Operation` many times

---

### Posterior Analysis (Runtime)

Depends on multiple parameters like:

1. **Hardware**
2. **Language**
3. **Compiler**
4. **Implementation**
5. etc

For posterior analysing the algorithm, we must **Run** it, then analyse it using tools like:

1. **IAR C-Run**
2. **Android Profiler**
3. etc

---
