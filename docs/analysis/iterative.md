# Iterative Algorithm

Algorithms without recursions called **Iterative**

```cpp
int fn(int value) {
    int sum = 0;
    while (value > 0) {
        sum += value;
    }

    return sum;
}
```

---

## Concepts

### Sigma Rules

There are many important sigma rules we will use in iterative algorithms analysis

$$
\begin{aligned}
    & \sum_{i=1}^{n} \frac{1}{i} = \frac{1}{1} + \frac{1}{2} + \frac{1}{3} + \dots + \frac{1}{n} = \ln{n}
    \\
    \\
    & \sum_{i=1}^{n} i = 1 + 2 + 3 + \dots + n = \frac{n.(n+1)}{2}
    \\
    \\
    & \sum_{i=1}^{n} i^2 = 1^2 + 2^2 + 3^2 + \dots + n^2 = \frac{n.(n+1).(2n+1)}{6}
    \\
    \\
    & \sum_{i=1}^{n} i^3 = 1^3 + 2^3 + 3^3 + \dots + n^3 = \frac{n^2.(n+1)^2}{4}
    \\
    \\
    & \sum_{i=0}^{n} ax^i = a + ax + ax^2 + \dots + ax^n = \frac{a(1-x^{n+1})}{1-x}
    \\
    \\
    & \sum_{i=0}^{\infty} ax^i = a + ax + ax^2 + \dots = \frac{a}{1-x}
\end{aligned}
$$

---

## Analysis

In most cases the **Space Complexity** and **Time Complexity** of an **Iterative Algorithm** is better than it's **Recursive Version**

Analysing an iterative algorithm is simple, just convert the loops into **Sigma** and simplify it

---

### Nested Loops

Finding a **Relation** for **nested loops** in iterative function

-   Convert each loop into a **Sigma**
-   Summation a **1** in last sigma

```js
const fn = (n) => {
    let x = 0;
    for (let i = 1; i <= n; i++) {
        for (let j = 1; j <= n; j++) {
            x++;
        }

        let j = 1;
        while (j < n) {
            x++;
            j *= 2;
        }
    }
};
```

$$
\begin{aligned}
    & T(n) = (\sum_{i=1}^{n} \sum_{j=1}^{n} 1) + (\sum_{i=1}^{n} \sum_{j=1}^{\log{n}} 1)
    \\ \implies
    & T(n) = (\sum_{i=1}^{n} n) + (\sum_{i=1}^{n} \log{n})
    \\ \implies
    & T(n) = n^2 + n.\log{n}
\end{aligned}
$$

---
