# Paradigm

**Algorithmic Paradigms** or **Algorithm of Algorithms** are general **ideas** and **techniques** we can use them to write algorithms

-   **Tip**: We can use **more than one** paradigm in one algorithm

There are two categories of problems, and for each one we have some paradigms:

-   **Primitive**: Problems with `primitive input`
-   **Non-Primitive**: Problems with `non-primitive input`

---

## Simple Recursion

In this method we write a **Recurrence Relation** for each `primitive problem` then we write a **Recursive Function** for solving the problem

We can write a recurrence relation for each problem using these steps:

1. Convert the problem into **Standard Problem** as **F(n)** (Infinite input size)
2. Find the simplest possible input as **Base Case** or **F(1)**
3. Try more **examples** as **F(2), F(3), F(4), ...**
4. Try to find a **relation** from **F(2), F(3), F(4), ...** to **F(1)**
5. Generalize this **relation**

---

-   **Tip**: Like divide & conquer, we can find a relation from $F(n)$ to $F(\frac{n}{k})$ to **optimize** the solution
-   **Tip**: When the relation doesn't have duplications in computation, the `complexity` of **Simple Recursion** and **Dynamic Programming** will be **equal**

---

-   **Example**: Find the N'th element of Fibonacci sequence

$$
\begin{aligned}
    & F(n) = F(n-1) + F(n-2)
    \\
    & F(0) = 0, \; F(1) = 1
\end{aligned}
$$

```ts
const recursive = (n: number) => {
    if (n === 0 || n === 1) {
        return n;
    }

    return recursive(n - 1) + recursive(n - 2);
};
```

---

## Dynamic Programming

In this method we **store** (`Memoization`) the **Previous Steps Answers** in order to `prevent duplicate of computation` and `optimizing` the recurrence relation solving, there are two versions of dynamic programming:

-   **Iterative (Bottom-Up)**: We start from **F(1)** to **F(n)** iteratively
-   **Recursive (Top-Down)**: We start from **F(n)** to **F(1)** recursively
    -   In this method we use **Memoization** in recursive function to prevent duplication of computation
    -   In **Memoization** we store function results in a **Cache Storage**

![Dynamic Programming](./assets/dynamic_programming.png)

---

-   **Example**: Find the N'th element of Fibonacci sequence

```ts
const iterative = (n: number) => {
    let cache = [0, 1];
    let index = 1;
    while (index++ <= n) {
        cache[index] = cache[index - 1] + cache[index - 2];
    }

    return cache[n];
};

let cache = [0, 1];
const recursive = (n: number) => {
    if (!(n in cache)) {
        cache[n] = recursive(n - 1) + recursive(n - 2);
    }

    return cache[n];
};
```

---

## Approximation

In this method we **approximate** the `output` more faster

![Approximation](assets/approximation.jpeg)

---

## Brute Force

In this method we **check** all the **Possible Scenarios** in order to find the correct one

![Brute Force](./assets/brute_force.png)

---

-   **Example**: Password cracker use a brute force method to check all possibilities
-   **Example**: Find number `k` in array `arr`: (**Linear Search**)

```ts
const solve = (arr: number[], k: number) => {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === k) {
            return true;
        }
    }

    return false;
};
```

---

## Divide & Conquer

In this method we **Divide** our **Problem** using a factor into **Sub-Problems** until get the **Base Case**, then **Solve** them and then **Merge** them:

1. **Divide/Break**: We break our **Problem** until getting smallest atomic sub-problems
2. **Conquer/Solve**: The solution of these **Smallest Atomic Sub-Problems** are obvious
3. **Merge/Combine**: After that we **Merge** our solved sub-problems into solved problem

![Divide & Conquer](./assets/divide_and_conquer.jpg)

---

-   **Tip**: We can write a **Recurrence Relation** for a divide & conquer algorithm **Complexity** using **Master Theorem**:

$$
\begin{aligned}
    & T(n) = a.T(\frac{n}{b}) + f(n)
    \\
    \\
    \\
    & a: \texttt{Number of sub-problems}
    \\
    \\
    & \frac{n}{b}: \texttt{Size of each sub-problem}
    \\
    \\
    & f(n): \texttt{Cost of dividing/merging}
\end{aligned}
$$

-   **Example**: We divide a problem by `5` and each partition has `1/3` of parent partition and the cost of dividing and merging is `O(n)`, what is the order of this solution?

$$
\begin{aligned}
    & T(n) = 5.T(\frac{n}{3}) + O(n)
    \\ \overset{Solve}{\implies}
    & T(n) \in O(n^{\log_3^5})
\end{aligned}
$$

---

-   **Tip**: The divided sub-problems may have **intersection**
-   **Example**: We have a group of `10` items, we divide this group into `3` groups with size of `4`

---

-   **Example**: Binary Search
-   **Example**: Merge Sort

---

## Greedy

In this method we **select** the **Best Possible Decision** in each step

![Greedy](./assets/greedy.gif)

1. Define the **Priority** of items
2. Choose the **Best Decision** at each step
3. If **Feasable**, `add it` to solution set
4. Else, `reject it` and don't consider it again
5. Keep repeating till you reach the end

---

-   **Tip**: This paradigm may `not give` the `optimial answer`
-   **Tip**: This paradigm may `not give` the `answer`

---

-   **Example**: Rate Monotonic Scheduling (Each step, run the shortest process)
-   **Example**: How to divide 1000 using 500, 100, 50, 10, 1 in lowest count (Monopoly banker)

```ts
const solver = (n: number) => {
    while (true) {
        if (n > 500) {
            // Select 500
        } else if (n > 100) {
            // Select 100
        } else if (n > 50) {
            // Select 50
        } else if (n > 10) {
            // Select 10
        } else if (n - 1 > 0) {
            // Select 1
        }
    }
};
```

---

## Back Tracking

In this method we **turn back** to **Decision Point** and **Make Another Decision** if we got dead end

![Back Tracking](./assets/back_tracking.png)

-   **Tip**: Selecting which **Decision Point** to return, is based on the algorithm we write

---

-   **Example**: DFS graph traverse
-   **Example**: BFS graph traverse

---

## Randomization

In this method we **randomize** the path from input to output (**Non-Deterministic Algorithm**), we can use this method in many ways:

1. **Shuffle Input** if we can (Sorting algorithms)
2. **Random Decision** if we have (DFS/BFS path selection)
3. any other part that can we randomize (Select, ...)

We can find the complexity of a **Randomized Algorithm** using **Probability Distributions**

---

-   **Example**: Randomized Quick Sort: $O(\log\log{n})$
-   **Example**: Check an array has duplicate values ?

```js
function hasDuplication(array) {
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < array.length; j++) {
            if (i !== j && array[i] === array[j]) {
                return true;
            }
        }
    }

    return false;
}
```

```js
function hasDuplication(array) {
    while (...) {
        const i = Math.random() * array.length;
        const j = Math.random() * array.length;

        if (i !== j && array[i] === array[j]) {
            return true;
        }
    }

    return false;
}
```

---

### Deterministic vs Non-Deterministic

There are two types of algorithms:

-   **Deterministic**: For one input one path to output (One complexity)
-   **Non-Deterministic**: For one input many pathes to output (Many complexities)
    -   Algorithm has a **Decision Making** part
    -   By better decisions, algorithm works better

![Deterministic NonDeterministic](./assets/deterministic_nondeterministic.png)

---

### Random Generator

Is a **hardware** used to generate real random numbers

There are two main ways to generate random numbers:

1. **Time Method**: A method generates fake random numbers using parameters
2. **Quantum Method**: A hardware generates really random numbers

![Quantum Random Generator](./assets/quantum_random_generator.png)

---

## Branch and Bound

We use this method to solve **Linear Problems**

---

-   **Example**: Simplex algorithm

---
