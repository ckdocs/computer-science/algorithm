# Tower of Hanoi

Tower of Hanoi, is a mathematical puzzle which consists of three towers (pegs) and more than one rings.

These rings are of different sizes and stacked upon in an ascending order, i.e. the smaller one sits over the larger one

-   **Goal**: We want to move the disks from **Tower 1** to **Tower 3** without violating the sequence of arrangement

![Tower of hanoi](../../assets/tower_of_hanoi.jpg)

We give the towers a name:

1. **Source**: Our disks are in that at the begining
2. **Auxiliary**: We use as a temporary tower
3. **Destination**: We want to move our disks to it

---

## Standard

In standard version of hanoi puzzle the rules are these:

1. Only **one disk** can move at a time
2. Only **the top disk** can move
3. Large disk **cannot** set over **small disk**

We can solve this problem in simplest mode with only **1**, **2**, **3** disks:

---

### One Disk

In this version our moves are these:

1. **Source** $\mapsto$ **Destination** (1 Disk)

---

### Two Disks

In this version our moves are these:

1. **Source** $\mapsto$ **Auxiliary** (1 Disk)
2. **Source** $\mapsto$ **Destination** (1 Disk)
3. **Auxiliary** $\mapsto$ **Destination** (1 Disk)

![Tower of Hanoi 2 Disks](../../assets/tower_of_hanoi_two_disks.gif)

---

### Three Disks

In this version our moves are these:

1. **Source** $\mapsto$ **Auxiliary** (2 Disk)
    1. **Source** $\mapsto$ **Destination** (1 Disk)
    2. **Source** $\mapsto$ **Auxiliary** (1 Disk)
    3. **Destination** $\mapsto$ **Auxiliary** (1 Disk)
2. **Source** $\mapsto$ **Destination** (1 Disk)
3. **Auxiliary** $\mapsto$ **Destination** (2 Disk)
    1. **Auxiliary** $\mapsto$ **Source** (1 Disk)
    2. **Auxiliary** $\mapsto$ **Destination** (1 Disk)
    3. **Source** $\mapsto$ **Destination** (1 Disk)

![Tower of Hanoi 2 Disks](../../assets/tower_of_hanoi_three_disks.gif)

---

### Simple Recursive

So in this **Simple Recursive** algorithm we break our problem into simple sub-problems

We cannot put **Largest disk** on any other disks, so we must move it directly from **Source** to **Destination** and other disks must be in **Auxiliary**

1. **Source** $\mapsto$ **Destination** (n Disks)
    1. **Source** $\mapsto$ **Auxiliary** (n-1 Disks)
    2. **Source** $\mapsto$ **Destination** (1 Disk)
    3. **Auxiliary** $\mapsto$ **Destination** (n-1 Disks)

```ts
function hanoi(
    sourceDisks: number,
    sourceName: string,
    auxiliaryName: string,
    destinationName: string
) {
    if (sourceDisks === 0) {
        return;
    }

    hanoi(sourceDisks - 1, sourceName, destinationName, auxiliaryName);
    console.log(`${sourceName} -> ${destinationName}`);
    hanoi(sourceDisks - 1, auxiliaryName, sourceName, destinationName);
}
```

Now we can write the **Complexity** of movements formula:

$$
\begin{aligned}
    & T(n) = 2.T(n-1) + 1
    \\
    & T(0) = 0
    \\
    \\ \overset{\texttt{Recurrence Tree}}{\implies}
    & T(n) = 1 + 2 + 4 + \dots = \sum_{i=0}^{n} 2^i = 2^n - 1
\end{aligned}
$$

---

#### Reduce Inputs

We can remove one **Tower Name** from arguments, we have 3 Towers, so by passing two of them we can find the third tower name, so only pass **Source** and **Destination** or any other two towers:

```ts
function hanoi(
    sourceDisks: number,
    sourceName: string,
    destinationName: string
) {
    let auxiliaryName = ...;
    ...
}
```

---

## Stricted

In stricted version of hanoi puzzle we cannot directly move between **Source** and **Destination**:

1. Only **one disk** can move at a time
2. Only **the top disk** can move
3. Large disk **cannot** set over **small disk**
4. We cannot move disks directly between **Source** and **Destination**
    - Use **Auxiliary** tower to move

We can solve this problem in simplest mode with only **1**, **2**, **3** disks:

---

### One Disk

In this version our moves are these:

1. **Source** $\mapsto$ **Destination** (1 Disk)
    1. **Source** $\mapsto$ **Auxiliary** (1 Disk)
    2. **Auxiliary** $\mapsto$ **Destination** (1 Disk)

---

### Two Disks

In this version our moves are these:

1. **Source** $\mapsto$ **Auxiliary** (1 Disk)
2. **Source** $\mapsto$ **Destination** (1 Disk)
    1. **Source** $\mapsto$ **Auxiliary** (1 Disk)
    2. **Auxiliary** $\mapsto$ **Destination** (1 Disk)
3. **Auxiliary** $\mapsto$ **Destination** (1 Disk)

But this steps are incorrect, because we are sitting a **Bigger Disk** in **Smaller Disk** in step **2.1**, so for fixing this problem we must move **Smaller Disks** before and after final move:

1. **Source** $\mapsto$ **Auxiliary** (1 Disk)
2. **Source** $\mapsto$ **Destination** (1 Disk)
    1. [**Auxiliary** $\mapsto$ **Destination**] (1 Disk)
    2. **Source** $\mapsto$ **Auxiliary** (1 Disk)
    3. [**Destination** $\mapsto$ **Auxiliary**] (1 Disk)
    4. [**Auxiliary** $\mapsto$ **Source**] (1 Disk)
    5. **Auxiliary** $\mapsto$ **Destination** (1 Disk)
    6. [**Source** $\mapsto$ **Auxiliary**] (1 Disk)
3. **Auxiliary** $\mapsto$ **Destination** (1 Disk)

---

### Simple Recursive

So in this **Simple Recursive** algorithm we break our problem into simple sub-problems

1. **Source** $\mapsto$ **Destination** (n Disks)
    1. **Source** $\mapsto$ **Destination** (n-1 Disks)
    2. **Source** $\mapsto$ **Auxiliary** (1 Disk)
    3. **Destination** $\mapsto$ **Source** (n-1 Disks)
    4. **Auxiliary** $\mapsto$ **Destination** (1 Disk)
    5. **Source** $\mapsto$ **Destination** (n-1 Disks)

```ts
function hanoi(
    sourceDisks: number,
    sourceName: string,
    auxiliaryName: string,
    destinationName: string
) {
    if (sourceDisks === 0) {
        return;
    }

    hanoi(sourceDisks - 1, sourceName, auxiliaryName, destinationName);
    console.log(`${sourceName} -> ${auxiliaryName}`);
    hanoi(sourceDisks - 1, destinationName, auxiliaryName, sourceName);
    console.log(`${auxiliaryName} -> ${destinationName}`);
    hanoi(sourceDisks - 1, sourceName, auxiliaryName, destinationName);
}
```

Now we can write the **Complexity** of movements formula:

$$
\begin{aligned}
    & T(n) = 3.T(n-1) + 2
    \\
    & T(0) = 0
    \\
    \\ \overset{\texttt{Recurrence Tree}}{\implies}
    & T(n) = 2 + 6 + 18 + \dots = 2.\sum_{i=0}^{n} 3^i = 3^n - 1
\end{aligned}
$$

---
