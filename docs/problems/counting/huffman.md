# Huffman Coding

A way to **Compress** a text, by changing the `characters coding` and `reduce` bits of `high frequency` characters

It the normal coding each character has size of **8 bit**

---

## Algorithm

-   Find the characters frequency
-   Create a **Priority Queue (Binary Min Heap)** using frequencies: $O(n)$
-   While queue is **Greater than 1**: $O(n.\log{n})$
    -   **Dequeue** two `min frequency` characters
    -   **Merge** together
    -   **Enqueue** to queue
-   **Traverse** tree, left is `0` and right is `1`

![Huffman Coding](../../assets/huffman_coding.png)

---

## Tips

-   **Tip**: Huffman tree is **Prefix Tree**, means each character coding cannot appear in the first of other characters
-   **Tip**: Huffman tree `cannot have` node with `single child` (It will not be optimized)
-   **Tip**: **Maximum** and **Minimum** `number of bits` for a character in huffman tree is **[1, n-1]**
-   **Tip**: Frequencies in the form of **Geometry Sequence** or **Fibonacci Sequence** generate `longest trees` with `n-1 bit` characters
-   **Tip**: If summation of each `two min` is `max` and numbers of characters is $2^k$ tree will be `full`, and characters coding bits are equals to normal coding, so `no compression applies`

$$
\begin{aligned}
    & \left.
    \begin{cases}
        Min + Min \implies Max
        \\
        \texttt{Characters count is } 2^k
    \end{cases}
    \right\} = \texttt{Full Tree} (\texttt{No compression})
    \\
    \\
    & Min + Min \implies Min : \texttt{Longest Tree}
\end{aligned}
$$
