# Largest Sum Contiguous Subarray

The problem of finding a **Sub-Array** in an array with `maximum` sum

![Largest Sum](../../assets/largest_sum.png)

---

## Divide & Conquer

-   **Divide** the given array in `two halves`
-   Return the **Maximum** of `following` three:
    -   **Maximum** subarray sum in `left half` (Make a `recursive` call): $T(\frac{n}{2})$
    -   **Maximum** subarray sum in `right half` (Make a `recursive` call): $T(\frac{n}{2})$
    -   **Maximum** subarray sum such that the `subarray` crosses the `midpoint`: $O(n)$

![Divide & Conquer](../../assets/largest_sum_divide_conquer.png)

```ts

```

**Complexity**: This recursive algorithm has the complexity bellow:

$$
\begin{aligned}
    & T(n) = 2T(\frac{n}{2}) + O(n)
    \\
    & T(1) = 1
    \\
    \\ \implies
    & T(n) \in \Theta(n.\log{n})
\end{aligned}
$$

-   **Time Complexity**: $O(n.\log{n})$
-   **Space Complexity**: $O(n.\log{n})$

---

## Kadane

**Complexity**: This algorithm has the complexity bellow:

$$
\begin{aligned}
    & T(n) \in \Theta(n)
\end{aligned}
$$

-   **Time Complexity**: $O(n)$
-   **Space Complexity**: $O(n)$

---
