# Scheduling

The problem of **Scheduling** `intervals` or tasks with `deadlines`

There are greedy methods for solving these problems, now we must define the priority of them

---

## Interval Scheduling

**Select** `maximum` number of intervals `without conflict`

There are some `intervals` $[s_i, f_i]$

$$
\begin{aligned}
    & s_i: \texttt{Start time}
    \\
    & f_i: \texttt{Finish time}
\end{aligned}
$$

![Interval Scheduling](../../assets/interval_scheduling.png)

-   **Earliest start time**: Sort by $s_i$ `ASC`: 🟥
    -   ![Earliest start time](../../assets/scheduling_earliest_start.png)
-   **Earliest finish time**: Sort by $f_i$ `ASC`: ✅
-   **Shortest interval**: Sort by $s_i - f_i$ `ASC`: 🟥
    -   ![Shortest interval](../../assets/scheduling_shortest_interval.png)
-   **Fewest conflicts**: Sort by number of **conflicts** `ASC`: 🟥
    -   ![Fewest conflicts](../../assets/scheduling_fewest_conflicts.png)

**Goal**: More free time resources:

-   The task that `frees resources` **Sooner** is better
-   The task that `allocates resources` **Later** is better

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n.\log{n} + n) = O(n.\log{n})$

There is an important tip:

-   **Maximize** the `intervals count`: Greedy ✅
-   **Maximize** the `intervals sum`: Greedy 🟥

---

## Scheduling All Intervals

**Finding** the `minimum` number of CPU's needs to process `intervals with conflict`

-   **Sort** intervals $s_i$ `ASC`: $O(n.\log{n})$
-   **Sort** intervals $f_i$ `ASC`: $O(n.\log{n})$
-   **Iterate** times and find `maximum` value of counter: $O(n)$
    -   If it was **Start**, `increase` counter
    -   If it was **Finish**, `decrease` counter

![Scheduling All Intervals](../../assets/scheduling_all_intervals.png)

$$
\begin{aligned}
    & \texttt{Counter}: \{1, 2, 1, 2, 3, 2, 3, 2, 3\}
    \\ \implies
    & Max(\texttt{Counter}) = \texttt{Needed CPU's}: 3
\end{aligned}
$$

-   **Tip**: We can also assign `CPU` to tasks by using a **Avail List** instead of counter and assign an **Available CPU** to the task instead of increasing counter

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n.\log{n} + n) = O(n.\log{n})$

---

## Job Scheduling With Deadline

**Select** `maximum` number of jobs so that the `maximum penalty` is `minimized`

There are some `jobs` $[t_i, d_i]$

$$
\begin{aligned}
    & t_i: \texttt{Job time}
    \\
    & d_i: \texttt{Job deadline}
\end{aligned}
$$

Each job that `pass` over the `deadline` has `penalty`

-   **Sort** jobs by $d_i$ `ASC`: $O(n.\log{n})$
-   **Iterate** jobs and select the possibles: $O(n)$

**Goal**: Do the job with `nearest deadline` first

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n.\log{n} + n) = O(n.\log{n})$

There is an important tip:

-   **Minimize** the `maximum penalty`: Greedy ✅
-   **Minimize** the `penalty sum`: Greedy 🟥

---

## Job Scheduling With Deadline, Profit

**Schedule** job to `maximize` the **Profit**, each job can have a `profit` if it `ends` until the `deadline`, each job's time is **1**

$$
\begin{aligned}
    & d_i: \texttt{Job deadline}
    \\
    & p_i: \texttt{Job profit}
\end{aligned}
$$

-   **Sort** jobs by $p_i$ `ASC`: $O(n.\log{n})$
-   **Iterate** jobs:
    -   **Find** the first empty element from $d_i$ to $1$ and place job: $O(n)$

![Job Scheduling With Deadline, Profit](../../assets/job_scheduling_deadline_profit.png)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity (Linear Search)**: $O(n.\log{n} + n^2) = O(n^2)$
-   **Time Complexity (Disjoint Sets)**: $O(n.\log{n} + n.\log{n}) = O(n.\log{n})$

**Optimization**: Finding the empty element for each job takes $O(n^2)$ using linear search, but we can optimize it using **Disjoint Sets** to $O(n.\log{n})$:

-   Each set **Agent** is the `first empty element`

![Optimized](../../assets/job_scheduling_deadline_profit_disjointsets.png)

---
