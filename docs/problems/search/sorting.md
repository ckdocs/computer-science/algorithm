# Sorting

The problem of **ordering elements** in **ascending** or **descending** order by **one or many fields** called sorting

![Sorting Orders](../../assets/sorting_orders.png)

---

## Concepts

There are two main category of sorting algorithms:

-   **Comparison Based**: Uses `comparison` operators to find the `order` between `two numbers`
    -   Selection Sort
    -   Insertion Sort
    -   Bubble Sort
    -   Tree Sort
    -   Heap Sort
    -   Shell Sort
    -   Quick Sort
    -   Merge Sort
-   **Other Approaches**:
    -   Counting Sort
    -   Bucket Sort
    -   Radix Sort

We can categorize sorting algorithms in many ways:

-   **Place**: Extra memory needed
-   **Stable**: Order of duplicate keys
-   **Adaptive**: Detect sorted and stop
-   **Storage**: Sort in RAM or Dist

There are some tips for sorting algorithms:

-   The **Insertion** method has **fewest** number of `swaps`
-   The **Comperable** methods cannot optimize better than $O(n.\log{n})$

---

### Place

1. **In-Place**: `No need` for extra array `space`: $O(1)$
2. **Out-Place**: `Needs` new array `space`: $O(n)$

---

### Stable

1. **Stable**: `Stable order` of `duplicate data`
2. **Un-Stable**: `Unstable order` of `duplicate data`

![Stable](../../assets/sorting_stable.webp)

-   **Tip**: We can convert any **Un-Stable** algorithm to **Stable** by **Comparing Index** of duplicated records with `same complexity`

---

### Adaptive

1. **Adaptive**: Algorithm can `detect` the array is `sorted` and `stop` the process
2. **Non-Adaptive**: Algorithm `cannot detect` array is `sorted` and process `until the end`

---

### Storage

1. **Internal**: The array is `stored` in `RAM`, and `sorted` in RAM
2. **External**: The array is `loaded` from `Disk`, and `partially sorted` and store in Disk

---

### Inversion

Occures when index of two elements is `increasing` but values are `decreasing` or reverse:

-   $i > j \implies A[i] < A[j]$
-   $i < j \implies A[i] > A[j]$

In sorting algorithms, we try to fix `inversions` in an `unsorted array`:

-   **Min**: $0$
-   **Max**: ${n \choose 2}$
-   **Mean**: $\frac{{n \choose 2}}{2}$

We can count the `inversions` of an `unsorted array`:

-   **Merge Sort**: In `merge` algorithm each time we select from `second array` we fix an inversion

---

### Decision Tree

**Comparison sorts** can be viewed `abstractly` in terms of **decision trees**. A decision tree is a **Full Binary Tree** that represents the `comparisons` between elements that are performed by a particular sorting algorithm operating on an input of a given size.

![Sorting Decision Tree](../../assets/sorting_decision_tree.png)

$$
\begin{aligned}
    & n: \texttt{Number of comparable items}
    \\
    \\
    & n!:
    \begin{cases}
        \texttt{Number of leafs}
        \\
        \texttt{All permutations of items}
    \end{cases}
    \\
    \\
    & \log{n!} = n.\log{n} =
    \begin{cases}
        \texttt{Minimum tree height}
        \\
        \texttt{Minimum number of compares}
    \end{cases}
    \\
    \\
    & \Omega(\lceil \log{n!} \rceil) = \Omega(n.\log{n}) = \texttt{Worst case complexity}
\end{aligned}
$$

---

## Selection Sort

An algorithm that **Selects** the **Maximum** in an `unsorted array` and place it at begining of a `sorted array`

![Sort Selection](../../assets/sort_selection.jpg)

-   Iterate `n-1` time:
    -   Find `maximum`
    -   Swap with `last`
    -   Reduce `length`

```ts
const selectionSort = (array: number[]) => {
    for (let i = 0; i < array.length; i++) {
        // Find max index
        // Reduce length by i
        let maxIndex = 0;
        for (let j = 0; j < array.length - i; j++) {
            if (array[j] > array[maxIndex]) {
                maxIndex = j;
            }
        }

        // Swap max with last
        swap(array[maxIndex], array[array.length - i - 1]);
    }
};
```

-   Number of **compares**:
    -   **Best Case**: $\frac{n.(n-1)}{2}$
    -   **Average Case**: $\frac{n.(n-1)}{2}$
    -   **Worst Case**: $\frac{n.(n-1)}{2}$
-   Number of **swaps**:
    -   **Best Case**: $n-1$
    -   **Average Case**: $n-1$
    -   **Worst Case**: $n-1$

---

## Insertion Sort

An algorithm that **Inserts** an **Unsorted Element** at its `suitable place` in each iteration

![Sort Insertion](../../assets/sort_insertion.png)

-   Iterate `n-1` time:
    -   Select `last element`
    -   Search in `sorted array`
    -   Shift `right`
    -   Insert into `suitable place`
    -   Reduce `length`

```ts
const insertionSort = (array: number[]) => {
    for (let i = 0; i < array.length; i++) {
        // Select last element
        const last = array[array.length - i - 1];

        // Search in sorted array
        for (let j = array.length - i; j < array.length; j++) {
            if (array[j] > last) {
                // Shift right and add
                shift(array[j]);
                array[j] = last;
                break;
            }
        }
    }
};
```

-   Number of **compares**:
    -   **Best Case**: $n-1$
    -   **Average Case**: $\frac{n.(n-1)}{2}$
    -   **Worst Case**: $\frac{n.(n-1)}{2}$
-   Number of **swaps**:
    -   **Best Case**: $0$
    -   **Average Case**: $n-1$
    -   **Worst Case**: $n-1$

---

## Bubble Sort

An algorithm that **Compares** the **Adjacent Elements** and `swaps` their positions if they are not in the intended order

![Sort Bubble](../../assets/sort_bubble.png)

-   Iterate `n-1` time:
    -   Iterate `n-i` time:
        -   Compare `left` and `right`
        -   Swap maximum to `right`
    -   Reduce `length`

```ts
const bubbleSort = (array: number[]) => {
    for (let i = 0; i < array.length; i++) {
        let swapped = false;

        // Iterate n-i time and swap adjacent elements
        for (let j = 0; j < array.length - i - 1; j++) {
            if (array[j] > array[j + 1]) {
                swap(array[j], array[j + 1]);

                swapped = true;
            }
        }

        // Adaptive stop of algorithm
        if (!swapped) {
            return;
        }
    }
};
```

-   Number of **compares**:
    -   **Best Case**: $n-1$
    -   **Average Case**: $\frac{n.(n-1)}{2}$
    -   **Worst Case**: $\frac{n.(n-1)}{2}$
-   Number of **swaps**:
    -   **Best Case**: $0$
    -   **Average Case**: $n-1$
    -   **Worst Case**: $n-1$

---

-   **Adaptive**: If the number of `swaps` in an iteration was `0` algorithm will stop (**Sorted**)

---

## Tree Sort

An algorithm that uses a **Binary Search Tree** for sorting, this algorithm is **Out-Place** and needs a memory of size $O(n)$ for storing `BST` structure

![Sort Tree](../../assets/sort_tree.png)

-   Create a `BST` with elements: $O(n \log{n})$
-   Traverse `BST` in `In-Order DFS`: $O(n)$

```ts
const treeSort = (array: number[]) => {
    // Create a BST with elements
    let bst = new BST(array);

    // Traverse BST in In-Order DFS
    return bst.inOrderDFS();
};
```

---

-   **Tip**: We can use a **Self-Balanced BST** like **AVL** or **Red-Black** to optimize the operation from $O(n^2)$ to $O(n \log{n})$

-   **Tip**: By storing the **Count** parameter, sorting **Equal Items** can done in complexity of $O(n.\log{k})$ where **k** is the number of unique numbers

---

## Heap Sort

An algorithm that uses a **Binary Heap Tree** for sorting

![Sort Heap](../../assets/sort_heap.png)

-   Create a `Heap` with elements: $O(n)$
-   Iterate `n` time:
    -   `Extract` the `max` or `min`: $O(\log{n})$

```ts
const heapSort = (array: number[]) => {
    // Create a Heap with elements
    let heap = new Heap(array);

    // Extract min or max from Heap
    for (let i = 0; i < array.length; i++) {
        array[i] = heap.extract();
    }
    return array;
};
```

---

-   **Tip**: If elements are **Equal** the **Extracting** complexity is $O(1)$ and **Sorting** complexity is $O(n)$

---

## Shell Sort

An algorithm that **Sorts** items with a `gap` then `reduce the gap`, it's generalized version of **Insertion Sort**

![Sort Shell](../../assets/sort_shell.png)

-   Define a `gap`
-   Sort elements of space `gap`
-   Reduce the `gap` until `1`

```ts
const shellSort = (array: number[]) => {
    // In each iteration reduce the gap
    for (let gap = array.length / 2; gap > 0; gap /= 2) {
        for (let i = gap; i < n; i += 1) {
            let temp = array[i];

            // Shift items
            let j;
            for (j = i; j >= gap && array[j - gap] > temp; j -= gap) {
                array[j] = array[j - gap];
            }

            // Set item to correct place
            array[j] = temp;
        }
    }
};
```

---

## Quick Sort

An algorithm based on **Divide & Conquer** paradigm, array is `partitioned` into two sub-arrays and **Recursively** call sort for these sub-arrays

![Sort Quick](../../assets/sort_quick.jpg)

-   Partition array using a `pivot`: $O(n)$
    -   Select an item as `pivot`
    -   Swap `smaller` items to `left of pivot`
    -   Swap `greater` items to `right of pivot`
-   Recursively sort `left sub-array`: $T(k)$
-   Recursively sort `right sub-array`: $T(n-k)$

```ts
const quickSort = (array: number[]) => {
    return sort(array, 0, array.length - 1);
};

const sort = (array: number[], low: number, high: number) => {
    if (low < high) {
        const pivot = partition(array, low, high);
        sort(array, low, pivot - 1);
        sort(array, pivot + 1, high);
    }
};

const partition1 = (array: number, low: number, high: number) => {
    // Select first item as pivot
    const pivot = array[low];

    // Swap smaller items to left sub-array
    let left = low;
    for (let right = low + 1; right <= high; right++) {
        if (array[right] < pivot) {
            left++;
            swap(array[left], array[right]);
        }
    }

    // Swap pivot to middle
    swap(array[low], array[left]);

    return left;
};

const partition2 = (array: number, low: number, high: number) => {
    // Select first item as pivot
    const pivot = array[low];

    let left = low;
    let right = high + 1;
    while (??) {
        // Increse left to find smaller item
        // Decrease right to find greater item
        if (left < right) {
            swap(array[left], array[right]);
        } else {
            swap(array[right], array[low]);
        }
    }

    return right;
};
```

-   Number of **compares**:
    -   **Best Case**: $n-1$
    -   **Average Case**: $\frac{n.(n-1)}{2}$
    -   **Worst Case**: $\frac{n.(n-1)}{2}$
-   Number of **swaps**:
    -   **Best Case**: $0$
    -   **Average Case**: $n-1$
    -   **Worst Case**: $n-1$

---

-   **Tip**: There are two cases for `partition`:
    -   **Best Case**: When `pivot` is `median`
        -   Pivot is `middle`
        -   Left side of size $\frac{n}{2}$
        -   Right side of size $\frac{n}{2}$
        -   Complexity is $O(n.\log{n})$
    -   **Worst Case**: When `pivot` is `min` or `max`
        -   Pivot is `left` or `right`
        -   Left side of size $0$
        -   Right side of size $n - 1$
        -   Complexity is $O(n^2)$

![Sort Quick Cases](../../assets/sort_quick_cases.png)

---

-   **Tip**: We can analyse the time complexity (Number of compares in `partition`) of `Quick Sort` using a formula depends on size of `Left sub-array` and `Right sub-array`:

$$
\begin{aligned}
    & T(n) = T(k) + T(n - k - 1) + n - 1
    \\
    & T(1) = 0
    \\
    \\ \implies
    & \texttt{Best Case}: T(n) = T(\frac{n}{2}) + T(\frac{n}{2}) + n - 1 \in \Theta(n.\log{n})
    \\ \implies
    & \texttt{Worst Case}: T(n) = T(0) + T(n-1) + n - 1 \in \Theta(n^2)
    \\ \implies
    & \texttt{Average}: T(n) =
    \begin{cases}
        T(0) + T(n-1)
        \\
        T(1) + T(n-2)
        \\
        T(2) + T(n-3)
        \\
        \vdots
        \\
        T(n-1) + T(0)
    \end{cases}
    \\
    & n.T(n) = \sum_{k=0}^{n-1} (T(k) + T(n - k - 1) + (n-1))
    \\ \implies
    & n.T(n) = \sum_{k=0}^{n-1} (T(k) + T(n - k - 1)) + n.(n-1)
    \\ \implies
    & n.T(n) = \sum_{k=0}^{n-1} (2.T(k)) + n.(n-1)
    \\ \implies
    & (n-1).T(n-1) = \sum_{k=0}^{n-2} (2.T(k)) + (n-1).(n-2)
    \\ \implies
    & n.T(n) - (n-1).T(n-1) = 2.T(n-1) + 2.(n-1)
    \\ \implies
    & T(n) = \frac{n+1}{n} T(n-1) + \frac{2.(n-1)}{n} \in \Theta(n.\log{n})
\end{aligned}
$$

---

### Randomized Quick Sort

In randomized version of quick sort, the **Partition** method will select **Pivot** `randomly`

```ts
...

const randomPartition = (array: number, low: number, high: number) => {
    // Select random item as pivot
    const pivot = array[Math.random(low, high)];

    ...
};
```

---

## Merge Sort

An algorithm based on **Divide & Conquer** paradigm, array is `splited` into two `half` sub-arrays and **Recursively** call sort for these sub-arrays, then these sorted halfs are `merged`, this algorithm is **Out-Place** and needs a memory of size $O(n)$ for storing `Merge` result array

![Sort Merge](../../assets/sort_merge.png)

-   Split array into two `halfs`: $O(1)$
-   Recursively sort `left sub-array`: $T(\lfloor \frac{n}{2} \rfloor)$
-   Recursively sort `right sub-array`: $T(\lceil \frac{n}{2} \rceil)$
-   Merge `sorted halfs`: $O(n)$
    -   Add `min` of `first elements` of arrays to result
    -   Add `remaining` items of `one array` to result

```ts
const mergeSort = (array: number[]) => {
    return sort(array, 0, array.length - 1);
};

const sort = (array: number[], low: number, high: number) => {
    if (low < high) {
        const middle = parseInt((low + high) / 2);

        const left = sort(array, low, middle);
        const right = sort(array, middle + 1, high);

        return merge(left, right);
    }

    return array;
};

const merge = (array1: number[], array2: number[]) => {
    let result = [];

    // Add min node of arrays to result
    let i = 0;
    let j = 0;
    while (i < array1.length || j < array2.length) {
        if (array1[i] < array2[j]) {
            // Min item selected from first array
            result.push(array1[i]);
            i++;
        } else {
            // Min item selected from second array
            result.push(array2[j]);
            j++;
        }
    }

    // Add remaining items of one array to result
    result.push(array1);
    result.push(array2);

    return result;
};
```

---

-   **Tip**: There are two cases for `merge`:
    -   **Best Case**: When `first array` is `after second`
        -   Number of `compares` is: $m + n - 1$
    -   **Worst Case**: When `min` element is decussate in `array1` and `array2`
        -   Number of `compares` is: $Min \{m, n\}$

---

-   **Tip**: We can analyse the time complexity (Number of compares in `merge`) of `Merge Sort` using a formula depends on size of `Left sub-array` and `Right sub-array`:

$$
\begin{aligned}
    & T(n) = T(\lfloor \frac{n}{2} \rfloor) + T(\lceil \frac{n}{2} \rceil) + O(n)
    \\
    & T(1) = 0
    \\
    \\ \implies
    & \texttt{Best/Average/Worst Case}: T(n) = T(\frac{n}{2}) + T(\frac{n}{2}) + O(n) \in \Theta(n.\log{n})
\end{aligned}
$$

---

-   **Tip**: We can merge `k` sorted lists with `n` items using a **Binary Min Heap** in: $O(n.\log{k})$
    -   Add first item of `k` lists to **Min Heap**
    -   Extract `min` from heap (From list `i`)
    -   Add another item of list `i` to heap

---

## Counting Sort

An algorithm that **Counts** the **Number** of occurrences of each unique **Number** in an `unsorted array` and store the counts in an `auxiliary array`, then `iterate` the auxiliary array

This algorithm has some **Conditions**:

-   Items of type `integer number`
-   Items in range of `[1, k]`

![Sort Counting](../../assets/sort_counting.png)

-   Find `max` value
-   Create `counts` array of size `max`
-   Iterate `array` and increase `counts[array[i]]` (`Ordinary count`)
-   Iterate `counts` and increase `counts[i] += counts[i-1]` (`Cummulative count`)
-   Iterate `array` from end and find the `index` of items by decreasing `cummulative count`

```ts
const countingSort = (array: number[]) => {
    // Find max
    let max = array[0];
    for (const item of array) {
        if (item > max) {
            max = item;
        }
    }

    // Create counts of size max
    let counts = new Array(max);

    // Find items ordinary counts
    for (const item of array) {
        counts[item]++;
    }

    // Find items cummulative counts
    for (let i = 1; i < counts.length; i++) {
        counts[i] += counts[i - 1];
    }

    // Find items indexes
    let result = new Array(array.length);
    for (let i = array.length - 1; i >= 0; i--) {
        result[counts[array[i]] - 1] = array[i];
        counts[array[i]]--;
    }

    return result;
};
```

---

## Bucket Sort

An algorithm that **Divides** items into **Buckets** in `sorted` order (Any algorithm), then prints buckets in order

This algorithm has some **Conditions**:

-   Items in range of `[0, 1)`

![Sort Bucket](../../assets/sort_bucket.png)

-   Create `k` buckets
-   Iterate `array` and map to `k`, add to bucket
-   Iterate `sorted buckets` and print items

```ts
const bucketSort = (array: number[]) => {
    // Create k buckets
    const k = 10;
    let buckets = new Array(k);
    for (let i = 0; i < k; i++) {
        buckets[i] = [];
    }

    // Add items to buckets
    for (const item of array) {
        buckets[item * 10].push(item);
    }

    // Iterate sorted buckets
    let result = [];
    for (const bucket of buckets) {
        result.push(Math.sort(bucket));
    }

    return result;
};
```

---

## Radix Sort

An algorithm that **Groups** the **Individual Digits** of the same place value, then sort the items

This algorithm has some **Conditions**:

-   Items of type `integer number`
-   Items at max `d` digits
-   Items in base of `r`

![Sort Radix](../../assets/sort_radix.png)

-   Iterate digits and **Stable Sort** items

```ts
const radixSort = (array: number[]) => {
    // Find max
    let max = array[0];
    for (const item of array) {
        if (item > max) {
            max = item;
        }
    }

    // Iterate digits and sort
    let digit = 1;
    while (max / digit > 0) {
        countingSort(array, digit);
        digit *= 10;
    }

    return array;
};
```

---

-   **Example**: What is the complexity of sorting `n` items of range `[0, n^4]` using:

    -   **Counting Sort**: $O(n+k) = O(n + n^4) = O(n^4)$
    -   **Radix Sort of base 10**: $O(d.(n+r)) = O(\log_{10}{(n^4) . (n + 10)}) = O(n.\log{n})$
    -   **Radix Sort of base n**: $O(d.(n+r)) = O(\log_{n}{(n^4) . (n + n)}) = O(n)$

---

## 3-Step Sort

An algorithm that sort **Slices** of an array iteratively

This algorithm has some **Conditions**:

-   `|B| >= |C|`

![Sort 3-Step](../../assets/sort_3step.png)

-   Sort `AB`
-   Sort `BC`
-   Sort `AB`

```ts
const threeStepSort = (array: number[]) => {
    const line = Math.ceil((2 * array.length) / 3);

    sort(array, 0, line - 1);
    sort(array, array.length - line, line - 1);
    sort(array, 0, line - 1);
};
```

-   **Order**: $O(n^{2.7})$

---
