# All-Pairs Shortest Paths

The problem of finding **Shortest Paths** between all the pairs of vertices in **Directed Connected Graphs**:

-   **Shortest Paths Tree**: Is a `spanning tree` of **Graph** `G`, such that the `path distance` from `root` to all vertices is `minimum`
-   **Tip**: In shortest path from **X** to **Y**, `no cycle` exists
-   **Tip**: `Maximum` path length of a shortest path is `n-1`
-   **Tip**: `Add` number to weights $\implies$ `SPT` may change
-   **Tip**: `Multiply` positive number to weights $\implies$ `SPT` not change

![Orders](../../assets/all_pairs_shortest_paths_orders.png)

---

## Concepts

### Distances, Parents

There are two important matrices, used in all **All-Pairs Shortest Paths** algorithms:

-   **Distances**: Shortest distances of two vertices
-   **Parents**: Shortest path parent of second vertex

---

## n-Times SPT

By running **SPT** method `n-times` for all `vertices` we can find **All-Pairs Shortest Paths**

-   **Bellman-Ford**: $O(n.e)$ $\implies$ $O(n^2.e)$
    -   **Bad Complexity**
-   **Dijkstra**: $O(e + n\log{n})$ $\implies$ $O(ne + n^2\log{n})$
    -   Not support **Negative Weights**

---

## Floyd-Warshall

Is a **Dynamic Programming** method for finding **Distances, Parents** matrices, in this method we want to find matrix $M_k$:

$$
\begin{aligned}
    & M_0 = \texttt{Adjacency Matrix}
    \\
    & M_1 = \texttt{Shortest path distances using vertex 1}
    \\
    & M_2 = \texttt{Shortest path distances using vertex 1, 2}
    \\
    \vdots
    \\
    & M_k = \texttt{Shortest path distances using vertex 1, 2, ..., k}
\end{aligned}
$$

First we must find the **Recurrence Relation** equivalent to the **Problem**:

$$
\begin{aligned}
    & M_k[i,j] = min
    \left.\begin{cases}
        M_{k-1}[i,j]
        \\
        M_{k-1}[i,k] + M_{k-1}[k,j]
    \end{cases}\right\}
    \\
    \\
    & M_0 \mapsto M_1 \mapsto M_2 \mapsto \dots M_k
\end{aligned}
$$

![Floyd Warshall](../../assets/all_pairs_shortest_paths_floyd.png)

```ts
const solve = (graph: number[][]) => {
    let distances = [];
    let parents = [];

    for (let k = 1; k < graph.length; k++) {
        for (let i = 0; i < graph.length; i++) {
            for (let j = 0; j < graph.length; j++) {
                if (graph[i][j] > graph[i][k] + graph[k][j]) {
                    // Path using k is shorter
                    distances[i][j] = graph[i][k] + graph[k][j];
                    // Set k as parent
                    parents[i][j] = k;
                } else {
                    // Direct path is shorter
                    distances[i][j] = graph[i][k] + graph[k][j];
                }
            }
        }

        graph = distances;
    }

    return m;
};

const printSolution = (parents: number[][], i: number, j: number) => {
    if (parents[i][j] !== 0) {
        printSolution(parents, i, parents[i][j]);
        print(parents[i][j]);
        printSolution(parents, parents[i][j], j);
    }
};
```

-   **Tip**: Floyd algorithm can solve with **Negative Weights**
-   **Tip**: Floyd algorithm can detect **Negative Cycles**:
    -   If the **Main Diagonal** cells of $M_k$ was **Negative**

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Adjacency Matrix**: $O(n^3)$
    -   **Tip**: It's better for **Dense Graphs**

---

### Path Existence

We can check, is there any path between **i** and **j** using **Floyd-Warshall**

We can use floyd with a customized **Recurrence Relation** optimized for only checking the path exists:

-   **Convert** weights matrix to **edge exists** matrix (`0, 1`)
-   **Set** main diagonal to `1`
-   Run **Floyd** algorithm with customized relation

$$
\begin{aligned}
    & M_k[i,j] = M_{k-1}[i,j] \;|\; (M_{k-1}[i,k] \;\&\; M_{k-1}[k,j])
\end{aligned}
$$

---

## Pseudo-Matrix Multiplication

Is a **Dynamic Programming** method for finding **Distances, Parents** matrices, in this method we want to find matrix $M^k$ by **Pseudo Multiplication** of $M^1$ (`Weights matrix`):

$$
\begin{aligned}
    & M^1 = \texttt{Adjacency Matrix}
    \\
    & M^2 = \texttt{Shortest path distances using at most 2 edges}
    \\
    \vdots
    \\
    & M^{k-1} = \texttt{Shortest path distances using at most k-1 edges}
    \\
    & M^k = \texttt{Shortest path distances using at most k edges}
\end{aligned}
$$

First we must find the **Recurrence Relation** equivalent to the **Problem**:

$$
\begin{aligned}
    & M^k[i,j] = min_{1 \leq k \leq n}\{M^{k-1}[i,k] + M^{1}[k,j]\}
\end{aligned}
$$

We call this method **Pseudo-Matrix Multiplication** because it's like matrix multiplication but instead of:

-   $\times$ $\implies$ $+$
-   $+$ $\implies$ $min$

![Pseudo-Matrix Multiplication](../../assets/all_pairs_shortest_paths_pseudo.png)

-   **Tip**: Floyd algorithm can solve with **Negative Weights**
-   **Tip**: In graphs **Without Negative Cycle**:
    -   $M^{k-1} = M^k = M^{k+1} = \dots$
-   **Tip**: Floyd algorithm can detect **Negative Cycles**:
    -   If $M^{k-1} \neq M^k$

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Naive**: $O(n^4)$
    -   **Optimized**: $O(n^3 \log{n})$

---

### Optimization

We can optimize **Pseudo-Matrix Multiplication** by:

$$
\begin{aligned}
    & M^k = M^{k-1} \times M^1
    \\ \implies
    & M^k[i,j] = min_{1 \leq k \leq n}\{M^{k-1}[i,k] + M^{1}[k,j]\}
    \\ \implies
    & O(n^4): \texttt{n times multiplication}
    \\
    \\
    & M^k = M^{\frac{k}{2}} \times M^{\frac{k}{2}}
    \\ \implies
    & min_{1 \leq k \leq n}\{M^{\lfloor \frac{k}{2} \rfloor}[i,k] + M^{\lceil \frac{k}{2} \rceil}[k,j]\}
    \\ \implies
    & O(n^3 \log{n}): \texttt{log(n) times multiplication}
\end{aligned}
$$

---

## Johnson

Is an **Improved** version of **n-Times Dijkstra** method supports **Negative Weights**:

-   **Reweight** edges to be `positive` (Without changing the **Shortest Paths**)
    -   **Add** vertex `S` to graph
    -   **Connect** vertex `S` with weight of `0` to all other vertices
    -   **Find** shortest paths tree from root `S` using `Bellman-Ford`
    -   **Update** edges weights:
    -   $$
        \begin{aligned}
            & w'(u,v) = w(u,v) + distance(S,u) - distance(S,v)
        \end{aligned}
        $$
-   **n-Times** Dijkstra
-   **Reverse Reweight** edges
    -   $$
        \begin{aligned}
            & \delta(u,v) = \delta'(u,v) - distance(S,u) + distance(S,v)
        \end{aligned}
        $$

![Johnson](../../assets/all_pairs_shortest_paths_johnson.png)

-   **Tip**: Johnson algorithm can solve with **Negative Weights**
-   **Tip**: Johnson algorithm cannot detect **Negative Cycles**

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Adjacency List**: $O(n \times \texttt{Dijkstra}) = O(ne + n^2\log{n})$
    -   **Tip**: It's better for **Sparse Graphs**

---
