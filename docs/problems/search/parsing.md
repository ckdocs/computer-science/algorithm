# Parsing

The problem of **Finding Tree** from a **Traversation** (Expression):

-   **Lexing**: Finding `Tokens` from `Traversation` using **Regular Expressions**
-   **Parsing**: Finding `AST` from `Tokens Sequence` using **Grammers**

![Lexer Parser](../../assets/lexer_parser.png)

For parsing we have two options:

-   Using token **Degrees** (Simple)
-   Using syntax **Grammers** (Compiler)

In this part we learn the **Degrees** way

---

## Concepts

Before the start, we must know about the important concepts in **Expressions** parsing:

1. **Expression**:
    - A tree **DFS** traversation (Prefix, Infix, Postfix)
    - A combination of **operands** using **operator** that can be **evaluated** and gets a value
2. **Operand**:
    - An **External Node (Leaf)** with **Degree = 0**
    - A constant or variable that **has a value**
3. **Operator**:
    - An **Internal Node** with **Degree > 0**
    - A function which **combines values** of operands

---

### Child Ambiguous

If a **Parent** wan't full, we must find the child **correct placement** under the parent, otherwise we cannot find a **Unique Tree**

![Left Right Operand](../../assets/left_right_operand.png)

-   **Tip**: In binary tree, the number of possible trees with **k** singular nodes:

$$
\begin{aligned}
    & \underbrace{2 \times 2 \times \dots \times 2}_{k} = 2^k
\end{aligned}
$$

---

### Infix Ambiguous

Infix notation has a bug, it may be **Ambiguous**, for example:

$$
\begin{aligned}
    & a \; + \; b \; * \; c
\end{aligned}
$$

In this expression we don't how to start evaluation **a + b** or **b \* c**

For fixing this bug we introduce three main concepts in **Infix Notation**:

1. **Parentheses**: Deepest parentheses must evaluate first
2. **Precedence**: Operator **priority**
   ![Precedence](../../assets/operators-precedence.jpg)
3. **Associativity**: Evaluate from **left** or **right**
   ![Associativity](../../assets/operators-associativity.jpg)

| Name                          | Operators                                   | Precedence     | Associativity     |
| ----------------------------- | ------------------------------------------- | -------------- | ----------------- |
| **Exponentiation**            | $\wedge \quad ** \quad ++ \quad -- \quad !$ | Highest        | **Right to Left** |
| **Multiplication & Division** | $* \quad / \quad \% \quad div \quad mod$    | Second Highest | **Left to Right** |
| **Addition & Subtraction**    | $+ \quad -$                                 | Lowest         | **Left to Right** |

---

## Degree Finding

If we doesn't have the token degrees, first we must find it using **Multiple Traversations** of a tree:

1. Try to find the **Root**
2. Find **Left** traversations
3. Find **Right** traversations
4. Recursively solve **Sub-Trees**

---

### InOrder, PreOrder

When we have **InOrder DFS** and **PreOrder DFS** traversations:

1. **Root**: `Pre-Order` **left** node
2. **Left**: Left of root in `In-Order`
3. **Right**: Right of root in `In-Order`

![InOrder PreOrder](../../assets/inorder_preorder_construct.png)

---

### InOrder, PostOrder

When we have **InOrder DFS** and **PostOrder DFS** traversations:

1. **Root**: `Post-Order` **right** node
2. **Left**: Left of root in `In-Order`
3. **Right**: Right of root in `In-Order`

![InOrder PostOrder](../../assets/inorder_postorder_construct.png)

---

### InOrder, LevelOrder

When we have **InOrder DFS** and **LevelOrder BFS** traversations:

For solving this problem:

1. **Root**: `Level-Order` **left-most** node exists in **In-Order**
2. **Left**: Left of root in `In-Order`
3. **Right**: Right of root in `In-Order`

![InOrder LevelOrder](../../assets/inorder_levelorder_construct.png)

---

### PreOrder, PostOrder

When we have **PreOrder DFS** and **PostOrder DFS** traversations:

1. **Root**:
    - `Pre-Order` **left** node
    - `Post-Order` **right** node
2. **Left**:
    - Begining of `Post-Order`
    - End at **root-successor** in `Pre-Order`, at `Post-Order`
3. **Right**:
    - End of `Pre-Order`
    - Begin at **root-predecessor** in `Post-Order`, at `Pre-Order`

![PreOrder PostOrder](../../assets/preorder_postorder_construct.png)

---

## Parsing

We have three main types of **DFS** traversation:

1. **Pre-Order (NLR)**: prefix notation (**Operator before**) (**Polish Notation** - PN)
2. **In-Order (LNR)**: infix notation (**Operator between**)
3. **Post-Order (LRN)**: postfix notation (**Operator after**) (**Reverse Polish Notation** - RPN)

| Prefix Notation | Infix Notation | Postfix Notation |
| --------------- | -------------- | ---------------- |
| + a b           | a + b          | a b +            |
| ∗ + a b c       | (a + b) ∗ c    | a b + c ∗        |
| ∗ a + b c       | a ∗ (b + c)    | a b c + ∗        |
| + / a b / c d   | a / b + c / d  | a b / c d / +    |

The general algorithm for parsing these notations is using a **Stack**:

```txt
Iterate RTL, LTR
If Degree = 0 (Operand): push
If Degree > 0 (Operator):
    pop by Degree
    merge
    push
```

---

### Prefix (Pre-Order)

We can use this algorithm for any conversation from **Prefix** to:

1. **AST**
2. **Infix**
3. **Postfix**

The general algorithm is:

```txt
Iterate from Right-to-Left
If Degree = 0 (Operand): push
If Degree > 0 (Operator):
    pop by Degree (Top is left)
    merge (AST, Infix, Postfix)
    push
```

```ts
const string = "--ab*cd";
let stack = [];

for (let token of string.reverse()) {
    if (token === `Operand`) {
        stack.push(token);
    } else if (token === `Operator`) {
        let left = stack.pop();
        let right = stack.pop();

        // Merge into AST
        stack.push({
            data: token,
            left: left,
            right: right,
        });

        // Merge into Infix
        stack.push(`( ${left} ${token} ${right} )`);

        // Merge into Postfix
        stack.push(`${left} ${right} ${token}`);
    }
}
```

---

### Infix (In-Order)

We can use this algorithm for any conversation from **Infix** to:

1. **AST**
2. **Prefix**:
    - ![Infix To Prefix](../../assets/infix_to_prefix.png)
3. **Postfix**:
    - ![Infix To Postfix](../../assets/infix_to_postfix.gif)

The general algorithm is:

```txt
Iterate from Left-to-Right
If Value = `(`: push
If Degree = 0 (Operand): push
If Degree > 0 (Operator): push
If Value = `)`:
    pop until get `(` (Top is right)
    merge (AST, Prefix, Postfix)
    push
```

```ts
const string = "(a+b)-c*d";
let stack = [];

for (let token of string) {
    if (token === "(") {
        stack.push(token);
    } else if (token === `Operand`) {
        stack.push(token);
    } else if (token === `Operator`) {
        stack.push(token);
    } else if (token === ")") {
        const right = stack.pop();
        const operator = stack.pop();
        const left = stack.pop();
        stack.pop();

        // Merge into AST
        stack.push({
            data: operator,
            left: left,
            right: right,
        });

        // Merge into Prefix
        stack.push(`${operator} ${left} ${right}`);

        // Merge into Postfix
        stack.push(`${left} ${right} ${operator}`);
    }
}
```

---

### Postfix (Post-Order)

We can use this algorithm for any conversation from **Postfix** to:

1. **AST**
    - ![Postfix Parentheses](../../assets/postfix_parentheses.png)
2. **Prefix**
3. **Infix**

The general algorithm is:

```txt
Iterate from Left-to-Right
If Degree = 0 (Operand): push
If Degree > 0 (Operator):
    pop by Degree (Top is right)
    merge (AST, Infix, Postfix)
    push
```

```ts
const string = "ab-cd*-";
let stack = [];

for (let token of string.reverse()) {
    if (token === `Operand`) {
        stack.push(token);
    } else if (token === `Operator`) {
        let right = stack.pop();
        let left = stack.pop();

        // Merge into AST
        stack.push({
            data: token,
            left: left,
            right: right,
        });

        // Merge into Prefix
        stack.push(`${token} ${left} ${right}`);

        // Merge into Infix
        stack.push(`( ${left} ${token} ${right} )`);
    }
}
```

---
