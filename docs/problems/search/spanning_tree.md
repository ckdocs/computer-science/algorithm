# Spanning Tree

**Spanning Tree** is tree, that `covers` all the vertices of a graph

![Orders](../../assets/spanning_tree_orders.png)

---

## Concepts

### Negative Cycle

Is a cycle with negative `sum of weights`, it's a bug in graph

For finding a **Shortest Path**, graph `should not have` a negative cycle

![Negative Cycle](../../assets/spanning_tree_negative_cycle.png)

---

## Minimum Spanning Tree

The problem of finding **Minimum Spanning Tree (MST)** in **UnDirected Connected Graphs**:

![Minimum Spanning Tree](../../assets/spanning_tree_mst.jpg)

-   **Minimum Spanning Tree**: Is a `spanning tree` with `minimum possible` **Sum Weight** of `edges`
-   **Tip**: In each `cut`, `lightest` edge were `unique` $\implies$ `MST` is `unique`
-   **Tip**: Edges `weights` are `unique` $\iff$ `MST` is `unique`
-   **Tip**: Edges `weights` are `not unique` $\implies$ `May` have `multiple MST's`
-   **Tip**: `Add` number to weights $\implies$ `MST` not change
-   **Tip**: `Multiply` positive number to weights $\implies$ `MST` not change
-   **Tip**: `Power` even number to positive weights $\implies$ `MST` not change
-   **Tip**: By adding an edge we can recompute the `MST` in $O(|V|)$
-   **Tip**: In complete graphs vertices the number of possible `MST's` is $n^{n-2}$

---

### Kruskal

Is a **Greedy** method to find the **MST** of a graph

-   **Sort** edges `ASC`: $O(e\log{e})$
-   **Create** `disjoint set`
-   For every `vertex`: $O(n)$
    -   **MakeSet** vertex: $O(1)$
-   For every `edge` u, v: $O(e)$
    -   If **FindSet** `u` and `v` is `not equal` (`No cycle`)
        -   **Union** `u` and `v`: $O(\alpha)$

![Kruskal](../../assets/spanning_tree_mst_kruskal.gif)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Adjacency List**: $O(e\log{e} + n + e\alpha) = O(e\log{e}) = O(e\log{n})$
    -   **Adjacency Matrix**: $O(e\log{n})$

---

### Boruvka

Is a **Greedy** method to find the **MST** of a graph

-   **Create** `disjoint set`
-   For every `vertex`: $O(n)$
    -   **MakeSet** vertex: $O(1)$
-   For every `vertex`: $O(n)$
    -   **Select** lowest weight `edge`
    -   If **FindSet** `u` and `v` is `not equal` (`No cycle`)
        -   **Union** `u` and `v`: $O(\alpha)$

![Boruvka](../../assets/spanning_tree_mst_boruvka.webp)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Adjacency List**: $O(e\log{n})$
    -   **Adjacency Matrix**: $O(e\log{n})$

---

### Prim

Is a **Greedy** method to find the **MST** of a graph

-   **Initialize** tree with a `root`
-   **While** `not cover` all vertices
    -   **Add** `lowest weight edge` of tree output neighbors
    -   **Check** `cycle` not created

The pseudo-code of this algorithm is:

-   TODO

![Prim](../../assets/spanning_tree_mst_prim.gif)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n \times ExtractMin + e \times DecreaseKey)$
    -   **Binary Min Heap + Adjacency List**: $O(n\log{n} + e\log{n}) = O(e\log{n})$
    -   **Fibonacci Heap + Adjacency List**: $O(n\log{n} + e) = O(e + n\log{n})$
    -   **Adjacency Matrix**: $O(n^2)$

---

### BFS, DFS

Is a way of finding **Spanning Tree** (`Not minimum`)

---

## Shortest Paths Tree

The problem of finding **Shortest Paths Tree (SPT)** of a **Vertex** in **Directed Connected Graphs**:

![Shortest Paths Tree](../../assets/spanning_tree_spt.jpg)

-   **Shortest Paths Tree**: Is a `spanning tree` such that the `path distance` from `root` to all vertices is `minimum`
-   **Tip**: `SPT` is not equals to `MST`
-   **Tip**: In shortest path from **X** to **Y**, `no cycle` exists
-   **Tip**: `Maximum` path length of a shortest path is `n-1`
-   **Tip**: `Add` number to weights $\implies$ `SPT` may change
-   **Tip**: `Multiply` positive number to weights $\implies$ `SPT` not change

There are two important arrays, used in all **SPT** algorithms:

-   **Distances**: Shortest distances of vertices from root
-   **Parents**: Shortest path parent of vertices

Now there are two methods for `initializing` and `updating` them:

-   **Init**: Initializing
    -   Set `distances` of `all vertices` to $\infty$
    -   Set `distance` of `root vertex` to $0$
    -   Set `parents` of `all vertices` to $NULL$
-   **Relax**: Update distance
    -   If this edge's path is `closer`
        -   `Update` distance, parent
-   **Relaxable**: Check distance updatable
    -   `Return` this edge's path is `closer`

```ts
const init = (graph: Graph, root: number) => {
    let distances = [0];
    let parents = [null];

    for (let i = 1; i < graph.verticesSize(); i++) {
        distances[i] = Number.MAX_VALUE;
        parents[i] = null;
    }

    return { distances, parents };
};

const relax = (from: number, to: number) => {
    if (distances[to] > distances[from] + weight(from, to)) {
        distances[to] = distances[from] + weight(from, to);
        parents[to] = from;
    }
};

const relaxable = (from: number, to: number) => {
    return distances[to] > distances[from] + weight(from, to);
};
```

---

### Bellman Ford

Is a **Brute Force** method to find the **SPT** for a **Root Vertex** in a graph with **Positive, Negative** weights

-   **Init** `distances` and `parents`: $O(n)$
-   For **n-1** times: $O(n)$
    -   **Relax** all edges: $O(e)$
-   For all **Edges**: $O(e)$
    -   If **Relaxable**, `negative cycle` exists: $O(1)$

![Bellman Ford](../../assets/spanning_tree_spt_bellman_ford.gif)

```ts
const solve = (graph: Graph, root: number) => {
    // Init distances, parents
    let { distances, parents } = init(graph, root);

    // Relax all edges n-1 times
    for (let i = 0; i < graph.verticesSize() - 1; i++) {
        for (const edge of graph.edges()) {
            relax(edge.from, edge.to);
        }
    }

    // Check negative cycle
    for (const edge of graph.edges()) {
        if (relaxable(edge.from, edge.to)) {
            throw "Negative Cycle!";
        }
    }

    return { distances, parents };
};
```

-   **Optimization**: In an iteration, if `no relax`, break the loop (**Adaptive**)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Adjacency List**: $O(n + ne + e) = O(ne)$
    -   **Adjacency Matrix**: $O(n + n^3 + n^2) = O(n^3)$

---

### Dijkstra

Is a **Greedy** method to find the **SPT** for a **Root Vertex** in a graph with **Positive** weights

-   **Initialize** tree with a `root`
-   **While** `not cover` all vertices
    -   **Relax** vertex `output neighbors`

It's like **Prim** algorithm, uses a `priority queue` and relax each edge `one time`

-   **Init** `distances` and `parents`: $O(n)$
-   **Create** `priority queue` with vertices of priority $\infty$: $O(n)$
-   **Reduce** `root` node priority to `0`
-   While queue not empty: $O(n)$
    -   **Pop** vertex: $O(\log{n})$
    -   **Find** all adjacents: $O(e)$
        -   **Reduce** key of `vertex` if `isRelaxable`

![Dijkstra](../../assets/spanning_tree_spt_dijkstra.gif)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n \times ExtractMin + e \times DecreaseKey)$
    -   **Binary Min Heap + Adjacency List**: $O(n\log{n} + e\log{n}) = O(e\log{n})$
    -   **Fibonacci Heap + Adjacency List**: $O(n\log{n} + e) = O(e + n\log{n})$
    -   **Adjacency Matrix**: $O(n^2)$

---

### DAG

In **Directed Acyclic Graphs** we can use a **Topological Sort** based method to find the **SPT**:

-   **Init** `distances` and `parents`: $O(n)$
-   **Sort** vertices in `topological order`: $O(e + n\log{n})$
-   For every `vertex`: $O(n)$
    -   For every `output neighbors`
        -   **Relax** adjacent edge

![DAG](../../assets/spanning_tree_spt_dag.png)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(e + n\log{n})$

---

### BFS

In **UnWeighted Graphs** we can also use **BFS** method to find the shortest path

-   **Init** `distances` and `parents`: $O(n)$
-   **BFS** vertices: $O(n + e)$
    -   **Relax** adjacent edge

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Adjacency List**: $O(n + e)$
    -   **Adjacency Matrix**: $O(n^2)$

---
