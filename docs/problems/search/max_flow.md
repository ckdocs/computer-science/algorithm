# Max Flow

The problem of finding **Max Flow** in **Flow Networks**

![Max Flow](../../assets/max_flow.png)

-   **Tip**: The `max flow path` is **Not Unique** even if `edges weights` be `unique`

---

## Concepts

### Flow Network

Is a **Directed Positive-Weighted Graph** that has a **Source** and **Target (Sink)**, the flow will exit from the source in enter into the target, we **Cannot Store** any flows in `vertices`

-   **Source**: Input degree is `0`
-   **Target (Sink)**: Output degree is `0`
-   **Flow**: The flow from `source` to `target`

Before running any algorithm on flow network we must **Standardize** the network:

-   **Anti Parallel Edges**: `Two way edges` must be converted two `two path edges`
    -   ![Anti Parallel Edges](../../assets/max_flow_anti_parallel_edges.png)
-   **Multiple Source-Target**: `Multiple` sources and targets must merged in one `source` and `target` with infinity
    -   ![Multiple Source-Target](../../assets/max_flow_anti_multiple_source_target.png)
-   **Weighted Vertices**: `Vertices` with capacity must converted into `two vertices` and `middle edge`
    -   ![Weighted Vertices](../../assets/max_flow_weighted_vertices.png)

---

### Residual Graph

**Residual Graph** or **Free Flow Graph** of graph $G$ Is a graph $G_f$ that flows are `reversed`

![Residual Graph](../../assets/max_flow_residual_graph.png)

---

### Augmented Path

Is a path we select in **Residual Graph** to pass the flow, there are two types of augmented paths:

-   **Good Path**: Path with `maximum` flow
-   **Bad Path**: Path with `minimum` flow

For optimizing the **Ford-Fulkerson** algorithm, we must `reduce the iterations` by selecting the **Good Path** in each iteration

![Augmented Path](../../assets/max_flow_augmented_path.png)

---

### Max-Flow Min-Cut Theorem

In a `Flow network`, the **Maximum** amount of **Flow** passing from the `source` to the `sink` is equal to the `total weight` of the edges in a **Minimum Cut**

![Max-Flow Min-Cut](../../assets/max_flow_min_cut.gif)

---

## Ford-Fulkerson

Is a **Greedy** method

-   Set **Flow** of all edged to `0`
-   **White** `augmented path` exists
    -   **Find** residual graph from $G$
    -   **Find** augmented path in $G_f$
    -   **Save** `lowest weight edge` of this path
    -   **Add** this weight to path edge `flows` in $G$
-   **Return** output flows of `S`

![Ford-Fulkerson](../../assets/max_flow_ford_fulkerson.jpg)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(e \times \texttt{MaxFlow})$

---
