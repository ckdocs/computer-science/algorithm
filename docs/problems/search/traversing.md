# Traversing

The problem of **searching** over a **Graph** or **Tree** with **O(n)**

There are two types of traversations:

-   **BFS**:
    -   `Level` order
    -   Using `queue`
-   **DFS**:
    -   `Depth` order
    -   Using `stack`

![Traversing](../../assets/traversing.png)

-   **Tip**: There are many versions of **DFS** and **BFS** in `trees` and `graphs`, they are different at **Children Selection** orders

---

## BFS

BFS or **Breadth-first search** or **Level order traversation**, implemented using a **Queue**:

1. **Pop** item from **queue** and **visit**
2. **Push** all **not-visited** children to **queue**

---

### Graph

The graph version of **BFS** traversation:

![Graph BFS](../../assets/traversing_bfs_graph.png)

```ts
const bfs = (graph: Graph) => {
    let visited = [];
    let queue = [graph[0]];

    while (queue.length > 0) {
        const vertex = queue.pop();

        visit(vertex);

        for (const neighbour of vertex.outputNeighbors()) {
            if (!visited[neighbour]) {
                queue.unshift(neighbour);
            }
        }
    }
};
```

-   **Tip**: If the graph isn't **Connected**, we must run **BFS** for `all` vertices`, to traverse all sub-graphs

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n + e)$
-   **Space Complexity**: $O(n + e)$

---

#### Edge Classification

-   **Tip**: The **BFS** doesn't have **Forward Edge**
-   **Tip**: The **BFS** have only **Tree Edge** and **Cross Edge**
-   **Tip**: The **BFS** in `undirected graph` doesn't have **Back Edge**

![Graph BFS Edges](../../assets/traversing_bfs_graph_edges.png)

---

### Tree

The tree version of **BFS** traversation:

![Tree BFS](../../assets/traversing_bfs_tree.png)

1. **level-order**: F, B, G, A, D, I, C, E, H

```ts
class Node {
    data: number;
    left?: Node;
    right?: Node;
}

const bfs = (node: Node) => {
    let queue = [node];

    while (queue.length > 0) {
        const item = queue.pop();

        visit(item);

        if (item.left && !visited(item)) {
            queue.unshift(item.left);
        }
        if (item.right && !visited(item)) {
            queue.unshift(item.right);
        }
    }
};
```

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n)$
-   **Space Complexity**: $O(n)$

---

## DFS

DFS or **Depth-first search** or **Depth order traversation**, implemented using a **Stack**

1. **Shift** item from **stack** and **visit**
2. **Push** all **not-visited** children to **stack**

---

### Graph

The graph version of **DFS** traversation:

![Graph DFS](../../assets/traversing_dfs_graph.png)

```ts
let visited = [];
let graph = new Graph();

const dfs = (vertex: Vertex) => {
    visit(vertex);

    for (const neighbour of vertex.outputNeighbors()) {
        if (!visited[neighbour]) {
            dfs(neighbour);
        }
    }
};
```

-   **Tip**: If the graph isn't **Connected**, we must run **DFS** for `all` vertices`, to traverse all sub-graphs

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n + e)$
-   **Space Complexity**: $O(n + e)$

---

#### Edge Classification

There are four classes of edges when traversing DFS:

-   **Tree Edge**: A `visited` edge
-   **Back Edge**: An `unvisited` edge from `child` to `parent`
-   **Forward Edge**: An `unvisited` edge from `parent` to `child`
-   **Cross Edge**: An `unvisited` edge from `no parent-child vertices`

![Graph DFS Edges](../../assets/traversing_dfs_graph_edges.png)

-   **Tip**: **Directed Graph** $\implies$ we may doesn't have **Tree Edge** based on the vertex `selection order`
-   **Tip**: **UnDirected Graph** $\implies$ `back edge` is equals to `forward edge`
-   **Tip**: **UnDirected Graph** $\implies$ `cross edge` doesn't exists
-   **Tip**: **Acyclic Graph** $\iff$ `back edge` doesn't exists

Assume we have `unvisited` edge **x->y**, we can discover the **Edge Type** using these methods when **DFS** traversation:

1. **Discovery Method**:
    - $d(y) \lt d(x) \lt f(x) \lt f(y)$ $\implies$ **Back Edge**
    - $d(x) \lt d(y) \lt f(y) \lt f(x)$ $\implies$ **Forward Edge**
    - $[d(x), f(x)] \cap [d(y), f(y)] = \emptyset$ $\implies$ **Cross Edge**
    - $d(x) \lt d(y) \lt f(x) \lt f(y)$ $\implies$ **Not Possible**
2. **Tricolor Method**:
    - **x=Gray** & **y=White** $\implies$ **Tree Edge**
    - **x=Gray** & **y=Gray** $\implies$ **Back Edge**
    - **x=Gray** & **y=Black** $\implies$ **Forward/Cross Edge**

---

#### Discovery Method

Each vertex has these additional properties:

-   **Discovery Time (d)**: Time the vertex `visited`
-   **Finish Time (f)**: Time the vertex `children visited`

$$
\begin{aligned}
    & \forall x \in V \;:\; 1 \leq d(x) \leq f(x) \leq 2n
\end{aligned}
$$

Now in DFS traversation we can save these properties using this method:

-   **Visit** vertex
-   **Set** `time++` to `discovery`
-   **Recursive** call for all `children`
-   **Set** `time++` to `finish`

![Graph DFS Discovery](../../assets/traversing_dfs_graph_discovery.png)

```ts
let discovery = [];
let finish = [];
let graph = new Graph();
let time = 0;

const dfs = (vertex: Vertex) => {
    visit(vertex);

    // Save discovery time
    discovery[vertex] = ++time;

    for (const neighbour of vertex.outputNeighbors()) {
        if (!discovery[neighbour]) {
            dfs(neighbour);
        }
    }

    // Save finish time
    finish[vertex] = ++time;
};
```

-   **Tip**: For validating a **Discovery** and **Finish** sequence, we can convert `discoveries` to **Open Parenthesis** and `finishes` to **Close Parenthesis**:
    -   ![Graph DFS Discovery Validate](../../assets/traversing_dfs_graph_discovery_validate.png)

---

#### Tricolor Method

Each vertex has these additional properties:

-   **Color**: The vertex `visited` state
    -   **White**: Vertex not `visited` yet
    -   **Gray**: Vertex `visited`
    -   **Black**: Vertex `children visited`

Now in DFS traversation we can save these properties using this method:

-   **Visit** vertex
-   **Set** color to `gray`
-   **Recursive** call for all `children`
-   **Set** color to `black`

![Graph DFS Tricolor](../../assets/traversing_dfs_graph_tricolor.png)

```ts
let color = [];
let graph = new Graph();

const dfs = (vertex: Vertex) => {
    visit(vertex);

    // Set to gray
    color[vertex] = "GRAY";

    for (const neighbour of vertex.outputNeighbors()) {
        if (!color[neighbour]) {
            dfs(neighbour);
        }
    }

    // Set to black
    color[vertex] = "BLACK";
};
```

---

### Tree

The tree version of **DFS** traversation:

![Tree DFS](../../assets/traversing_dfs_tree.png)

1. **pre-order** (red): F, B, A, D, C, E, G, I, H
2. **in-order** (yellow): A, B, C, D, E, F, G, H, I
3. **post-order** (green): A, C, E, D, B, H, I, G, F

```ts
class Node {
    data: number;
    left?: Node;
    right?: Node;
}

const dfs = (node: Node) => {
    if (!node || visited(node)) {
        return;
    }

    // LNR
    dfs(node.left);
    visit(node);
    dfs(node.right);
};
```

-   **Tip**: `Leafs` order in all the **DFS** traversations are **same**
-   **Tip**: If you see `...xy...` in **Pre-Order** and `...yx...` in **Post-Order**:
    -   **x** has one child **y**

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n)$
-   **Space Complexity**: $O(n)$

---

#### Pre-Order

The algorithm of **Iterative Direct Pre-Order** is:

-   **Create** stack with `root` node
-   While `stack` is **Not Empty**:
    -   **Pop** and `print`
    -   **Push** `right` child
    -   **Push** `left` child

There are two versions of **Pre-Order**:

-   **Direct (NLR)**: Move from `left` to `right`

    ```js
    // Recursive
    const dfs = (node: Node) => {
        if (!node || visited(node)) {
            return;
        }

        visit(node);
        dfs(node.left);
        dfs(node.right);
    };

    // Iterative
    const dfs = (node: Node) => {
        let stack = [node];

        while (stack.length > 0) {
            const item = stack.pop();

            visit(item);

            if (item.right && !visited(item)) {
                stack.push(item.right);
            }
            if (item.left && !visited(item)) {
                stack.push(item.left);
            }
        }
    };
    ```

-   **Reverse (NRL)**: Move from `right` to `left`

    ```js
    visit(node);
    dfs(node.right);
    dfs(node.left);
    ```

---

#### In-Order

The algorithm of **Iterative Direct In-Order** is:

-   **Create** `empty` stack
-   While `stack` is **Not Empty** or `current node` is **Not Null**:
    -   **Move** current node to `left most` and **Push** to `stack`
    -   **Pop** and `print`
    -   **Move** to `right`

There are two versions of **In-Order**:

-   **Direct (LNR)**: Move from `left` to `right`

    ```js
    // Recursive
    const dfs = (node: Node) => {
        if (!node || visited(node)) {
            return;
        }

        dfs(node.left);
        visit(node);
        dfs(node.right);
    };

    // Iterative
    const dfs = (node: Node) => {
        let stack = [];

        while (node !== null || stack.length > 0) {
            // Go to left
            while (node !== null) {
                stack.push(node);
                node = node.left;
            }

            node = stack.pop();
            visit(node);
            node = node.right;
        }
    };
    ```

-   **Reverse (RNL)**: Move from `right` to `left`

    ```js
    dfs(node.right);
    visit(node);
    dfs(node.left);
    ```

---

##### Skewed Binary Tree

Traversing skewed binary tree has two tips:

1. **In-Order DFS** = **Pre-Order DFS** $\iff$ **Right Skewed Binary Tree**
2. **In-Order DFS** = **Post-Order DFS** $\iff$ **Left Skewed Binary Tree**

![Skewed Binary Tree](../../assets/traversing_dfs_skewed_tree.png)

---

##### Threaded Binary Tree

By threading **NULL links** of a binary tree, to top **Parent**, instead of using **Stack** or **Recursive** we can use an **Iterative** algorithm for **In Order** traversing (Direct, Reverse):

For each **NULL links**:

1. **Left NULL**: Connect to the top **first** node, i'm on **right of it** (**Predecessor**)
2. **Right NULL**: Connect to the top **first** node, i'm on **left of it** (**Successor**)

![Threaded Binary Tree](../../assets/traversing_dfs_threaded_tree.png)

-   **Time Complexity**: $O(n)$
-   **Space Complexity**: $O(1)$

---

#### Post-Order

The algorithm of **Iterative Direct Post-Order** is:

-   **Create** stack1 with `root` node
-   **Create** `empty` stack2
-   While `stack1` is **Not Empty**:
    -   **Pop** and push to `stack2`
    -   **Push** `left` to `stack1`
    -   **Push** `right` to `stack1`

There are two versions of **Post-Order**:

-   **Direct (LRN)**: Move from `left` to `right`

    ```js
    // Recursive
    const dfs = (node: Node) => {
        if (!node || visited(node)) {
            return;
        }

        dfs(node.left);
        dfs(node.right);
        visit(node);
    };

    // Iterative
    const dfs = (node: Node) => {
        let stack1 = [node];
        let stack2 = [];

        while (stack1.length > 0) {
            const item = stack1.pop();

            stack2.push(item);

            if (item.left) {
                stack1.push(item.left);
            }
            if (item.right) {
                stack1.push(item.right);
            }
        }

        print(stack2);
    };
    ```

-   **Reverse (RLN)**: Move from `right` to `left`

    ```js
    dfs(node.right);
    dfs(node.left);
    visit(node);
    ```

---
