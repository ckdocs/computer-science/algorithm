# Selecting

The problem of **finding k-th min,max** in a **List** called `searching` or `finding statistical orders`

![Selecting Orders](../../assets/selecting_orders.png)

-   **Tip**: All of these operations (`Min`, `Max`) can complete in $\Theta(n)$

---

## Concepts

-   $n\texttt{-th Min} = 1\texttt{-th Max}$
-   $n\texttt{-th Max} = 1\texttt{-th Min}$
-   $\frac{n}{2}\texttt{-th Min} = \frac{n}{2}\texttt{-th Max}$ = $\texttt{Median}$
-   $k\texttt{-th Min} = k\texttt{-th Statistical Order}$

---

## 1-th Min

### Iterate Method

-   Set `A[0]` as `min`
-   Iterate `n` items: $O(n)$
    -   If smaller than `min`, update min

```ts
const firstMin = (array: number[]) => {
    // Set A[0] as min
    let min = array[0];

    // Iterate items and update min
    for (const item of array) {
        if (item < min) {
            min = item;
        }
    }

    return min;
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = n-1
\end{aligned}
$$

---

## 2-th Min

### Iterate Method

-   Find `1-th min` and remove it: $O(n)$
-   Find `2-th min` and return it: $O(n)$

```ts
const secondMin = (array: number[]) => {
    // Find 1-th min and remove
    let firstMin = firstMin(array);

    return firstMin(array.remove(firstMin));
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = (n-1) + (n-2)
\end{aligned}
$$

---

### Tournament Method

-   Find `1-th min` by comparing doubly in `tournament`: $O(n)$
-   Find min of `1-th min loosers` list: $O(\log{n})$

![Tournament Method](../../assets/selecting_tournament_method.png)

```ts
const secondMin = (array: number[]) => {
    ...
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = (n-1) + (\log{n}-1)
\end{aligned}
$$

---

## k-th Min

### Iterate Method

-   Find `1-th min` and remove it: $O(n)$
-   Find `2-th min` and remove it: $O(n)$
-   ...
-   Find `k-th min` and return it: $O(n)$

```ts
const kthMin = (array: number[], k: number) => {
    let mins = [];

    // Remove mins 1,2,...,k-1
    mins[0] = firstMin(array.remove(mins));
    mins[1] = firstMin(array.remove(mins));
    ...
    mins[k-2] = firstMin(array.remove(mins));

    // Return min k
    return firstMin(array.remove(mins));
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = (n-1) + (n-2) + (n-3) + \dots + (n-k)
\end{aligned}
$$

---

### Sort Method

-   Sort array: $O(n.\log{n})$
-   Select `k-th` item

```ts
const kthMin = (array: number[], k: number) => {
    // Sort array
    let sorted = sort(array);

    return sorted[k];
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = n.\log{n}
\end{aligned}
$$

---

### Heap Method

-   Create a `min heap`: $O(n)$
-   Extract min `k` time: $O(k.\log{n})$

```ts
const kthMin = (array: number[], k: number) => {
    // Create min heap
    const heap = new BinaryMinHeap(array);

    // Extract min, k times
    let min = 0;
    while (k > 0) {
        min = heap.extract();
    }

    return min;
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = O(n) + k.\log{n}
\end{aligned}
$$

---

### Partition Method

-   Partition array and get `pivot index`: $O(n)$
-   If `pivotIndex = k`: return pivot
-   If `pivotIndex > k`: find min in `left`: $T(n-1)$
-   If `pivotIndex < k`: find min in `right`: $T(n-1)$

```ts
const kthMin = (array: number[], left: number, right: number, k: number) => {
    // Paritition array
    const pivotIndex = partition(array, left, right);

    // Compare pivotIndex with k
    if (pivotIndex === k) {
        return array[pivotIndex];
    } else if (pivotIndex > k) {
        return kthMin(array, left, k - 1);
    } else {
        return kthMin(array, k + 1, right);
    }
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) =
    \begin{cases}
        T(\frac{n}{2}) + n = 2n-1 & \texttt{Best Case}
        \\
        T(n-1) + n = \frac{n.(n+1)}{2} & \texttt{Worst Case}
    \end{cases}
\end{aligned}
$$

---

### Median Partition Method

This method is like to `Partition Method`, but in `partitioning` we select `median` as `pivot`:

-   Partition method:
    -   Split array into `n/5` parts with size of `5`: O(1)
    -   Find `median` in each part: $O(\frac{n}{5})$
    -   Find `median` of medians as `pivot`: $T(\frac{n}{5})$
    -   Parition using `pivot`: $O(n)$
-   If `pivotIndex = k`: return pivot
-   If `pivotIndex > k`: find min in `left`: $Max\{T(\frac{3n}{10}), \frac{7n}{10})\}$
-   If `pivotIndex < k`: find min in `right`: $Max\{T(\frac{3n}{10}), \frac{7n}{10})\}$

![Median Method](../../assets/selecting_median_method.png)

```ts
const partition = (array: number[], left: number, right: number) => {
    const parts = Math.ceil((right - left + 1) / 5);

    // Find median of partitions
    let medians = [];
    for (let i = 0; i < parts; i++) {
        medians[i] = kthMin(array, i * 5, (i + 1) * 5 - 1, 3);
    }

    // Find median of medians
    const pivot = kthMin(medians, 0, medians.length - 1, medians.length / 2);

    // Partition
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = T(\frac{7n}{10}) + T(\frac{n}{5}) + O(n)
    \\ \implies
    & T(n) \in \Theta(n)
\end{aligned}
$$

---

## Min & Max

### Iterate Method

-   Set `min`, `max` as `A[0]`
-   Iterate `n` items: $O(n)$
    -   If smaller than `min`, update min
    -   If greater than `max`, update min

```ts
const minMax = (array: number[]) => {
    // Set min,max as A[0]
    let min = array[0];
    let max = array[0];

    // Iterate items and update min
    for (const item of array) {
        if (item < min) {
            min = item;
        } else if (item > max) {
            max = item;
        }
    }

    return [min, max];
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) =
    \begin{cases}
        n-1 & \texttt{Best Case}
        \\
        2n-1 & \texttt{Worst Case}
    \end{cases}
\end{aligned}
$$

---

### Divide Method

-   Divide array into two parts
-   Find `min,max` in `left` and `right`: $2.T(\frac{n}{2})$
-   Find `min` between `left min` and `right min`: $O(1)$
-   Find `max` between `left max` and `right max`: $O(1)$

![Divide Method](../../assets/selecting_divide_method.png)

```ts
const minMax = (array: number[], left: number, right: number) => {
    if (right - left === 0) {
        // If one item, return as min,max
        return [array[left], array[left]];
    }

    // Find min,max in left, right
    const middle = (right - left) / 2;
    const [min1, max1] = minMax(array, left, middle);
    const [min2, max2] = minMax(array, middle, right);

    // Find final min,max
    return [Math.min(min1, min2), Math.max(max1, max2)];
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = T(\lfloor \frac{n}{2} \rfloor) + T(\lceil \frac{n}{2} \rceil) + 2
    \\
    & T(2) = 1
    \\
    & T(1) = 0
    \\
    \\ \implies
    & T(n) \in \Theta(n)
\end{aligned}
$$

---

### Pair Method

-   Pair each two `following items`
-   Find `min,max` in each `pair`: $O(\frac{n}{2})$
-   Find `min` in `mins` array: $O(\frac{n}{2} - 1)$
-   Find `max` in `mixes` array: $O(\frac{n}{2} - 1)$

![Pair Method](../../assets/selecting_pair_method.png)

```ts
const minMax = (array: number[]) => {
    let mins = [];
    let maxes = [];

    // Find min,max in each pair
    for (let i = 0; i < array.length; i += 2) {
        if (array[i] < array[i + 1]) {
            mins.push(array[i]);
            maxes.push(array[i + 1]);
        } else {
            mins.push(array[i + 1]);
            maxes.push(array[i]);
        }
    }

    // Find final min
    let min = mins[0];
    for (const item of mins) {
        if (item < min) {
            min = item;
        }
    }

    // Find final max
    let max = maxes[0];
    for (const item of maxes) {
        if (item > max) {
            max = item;
        }
    }

    return [min, max];
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = \lceil \frac{3n}{2} \rceil - 2 =
    \begin{cases}
        \frac{3n}{2}-2 & \texttt{n is Even}
        \\
        \frac{3n}{2}-\frac{3}{2} & \texttt{n is Odd}
    \end{cases}
\end{aligned}
$$

---

## 1-th to k-th Mins

### Iterate Method

-   Find `1-th min` and save it: $O(n)$
-   Find `2-th min` and save it: $O(n)$
-   ...
-   Find `k-th min` and save it: $O(n)$

```ts
const kMins = (array: number[], k: number) => {
    let mins = [];

    // Remove mins 1,2,...,k
    for (let i = 0; i < k; i++) {
        mins[i] = firstMin(array.remove(mins));
    }

    return mins;
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = (n-1) + (n-2) + (n-3) + \dots + (n-k)
\end{aligned}
$$

---

### Sort Method

-   Sort array: $O(n.\log{n})$
-   Select items `1..k`

```ts
const kMins = (array: number[], k: number) => {
    // Sort array
    let sorted = sort(array);

    return sorted.splice(0, k - 1);
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = n.\log{n}
\end{aligned}
$$

---

### Partition Method

-   Partition array using `k-th min`
-   Return `left` of `pivot`

```ts
const kMins = (array: number[], k: number) => {
    // Paritition array
    const pivotIndex = partition(array, k);

    return array.splice(0, pivotIndex);
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & T(n) = O(n)
\end{aligned}
$$

---
