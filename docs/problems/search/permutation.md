# Permutation

The problem of finding **Permutation** of `n` items

![Permutation](../../assets/permutation.gif)

---

## With replacement

```ts
function perm(alphabet: string, result: string) {
    if (alphabet === "") {
        console.log(result);
    }

    for (let i = 0; i < alphabet.length; i++) {
        let alphabetWithoutI =
            alphabet.substring(0, i) +
            alphabet.substring(i + 1, alphabet.length);

        perm(alphabetWithoutI, result + alphabet[i]);
    }
}

perm("123", "");
/*
    123;
    132;
    213;
    231;
    312;
    321;
*/
```

-   **Time Complexity**: $O(n!)$
-   **Space Complexity**: $O(n!)$

---

## Without replacement

```ts
function perm(alphabet: string, result: string) {
    if (alphabet === "") {
        console.log(result);
    }

    perm(alphabet.substring(1, alphabet.length), result);
    perm(alphabet.substring(1, alphabet.length), result + alphabet[0]);
}

perm("123", "");
/*
    123;
    12;
    13;
    23;
    3;
    2;
    1;
*/
```

-   **Time Complexity**: $O(2^n)$
-   **Space Complexity**: $O(2^n)$

---
