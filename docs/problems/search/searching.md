# Searching

The problem of **finding an element** in a **Data Structure** called searching

![Searching Orders](../../assets/searching_orders.png)

---

## Concepts

### Unknown Length

If array has unknows length:

1. Start from index **0**
2. **Doubly** index each step while **Element > Current**
3. If **Element < Current**: Binary search **[Current/2, Current]**

We can **Search** in this array with **Complexity** of $O(\log{n})$

---

### Bitonic Array

If array has a **Max**:

1. Find **Max** in binary
2. Find **Left, Right** in binary

![Bitonic Array](../../assets/bitonic_array.png)

We can **Search** in this array with **Complexity** of $O(\log{n})$

---

### Rotated Array

Is a sorted array which is rotated:

1. Find **Max** where right is **Min** in binary
2. Find **Left, Right** in binary

![Rotated Array](../../assets/rotated_array.jpg)

We can **Search** in this array with **Complexity** of $O(\log{n})$

---

### Infimum and Supremum

Is a radial array:

1. Find **Max** where left and right is **Min** in binary

We can **Search** in this array with **Complexity** of $O(\log{n})$

---

### Sorted Matrix

We can **Linear Search** in a sorted matrix with **Complexity** of $O(n)$

---

## Linear Search

1. Iterate elements from start to end
2. Check each one

![Linear Search](../../assets/linear_search.png)

Is an algorithm using these paradigms:

1. **Brute Force**

```ts
const solve = (array: number[], element: number) => {
    for (const item of array) {
        if (item === element) {
            return true;
        }
    }

    return false;
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & \texttt{Best Case}: O(1)
    \\
    & \texttt{Worst Case}: O(n)
    \\
    & \texttt{Average Case Successful}: S(n) = \sum_{i=1}^{n} P(i).T_s(i)
    \\ \implies
    & \frac{1}{n}.1 + \frac{1}{n}.2 + \dots + \frac{1}{n}.n = \frac{1 + 2 + \dots + n}{n} = \frac{n.(n+1)}{2n} = \frac{n+1}{2}
    \\
    \\
    & \texttt{Average Case UnSuccessful}: U(n) = \sum_{i=1}^{n+1} P(i).T_u(i)
    \\ \implies
    & \frac{1}{n+1}.n + \frac{1}{n+1}.n + \dots + \frac{1}{n+1}.n = \frac{n + n + \dots + n}{n+1} = \frac{n.(n+1)}{n+1} = n
    \\
    \\
    \\
    & P(i): \texttt{Probability of searching item at index i}
    \\
    & T_s(i): \texttt{Number of compares needs to find successful item at index i}
    \\
    & T_u(i): \texttt{Number of compares needs to find unsuccessful item at index i}
\end{aligned}
$$

-   Relation of **Successful** and **UnSuccessful** compares:

$$
\begin{aligned}
    & (n+1).U(n) = n.S(n) + n
\end{aligned}
$$

---

## Binary Search

This algorithm will work on **Sorted Arrays**

1. Devide array into **two parts**
2. Check **center**
3. If **greater**: check **left** sub-array
4. If **smaller**: check **right** sub-array

![Binary Search](../../assets/binary_search.png)

Is an algorithm using these paradigms:

1. **Divide & Conquer**

```ts
const solve = (array: number[], element: number) => {
    let low = 0;
    let high = array.length - 1;

    while (low <= high) {
        let middle = (low + high) / 2;

        if (array[middle] === element) {
            return true;
        }

        if (array[middle] > element) {
            // Check left
            high = middle - 1;
        } else {
            // Check right
            low = middle + 1;
        }
    }

    return false;
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & \texttt{Best Case}: O(1)
    \\
    & \texttt{Worst Case}: O(\log{n})
    \\
    & \texttt{Average Case Successful}: S(n) = \sum_{i=1}^{n} P(i).T_s(i)
    \\
    & \texttt{Average Case UnSuccessful}: U(n) = \sum_{i=1}^{n+1} P(i).T_u(i)
    \\
    \\
    & P(i): \texttt{Probability of searching item at index i}
    \\
    & T_s(i): \texttt{Number of compares needs to find successful item at index i}
    \\
    & T_u(i): \texttt{Number of compares needs to find unsuccessful item at index i}
\end{aligned}
$$

![Searching Compares](../../assets/searching_compares.png)

$$
\begin{aligned}
    & S(n) = \frac{3 + 2 + 3 + 4 + 1 + 3 + 2 + 3 + 4}{9} = \frac{25}{9}
    \\
    & U(n) = \frac{3 + 3 + 3 + 4 + 4 + 3 + 3 + 3 + 4 + 4}{10} = \frac{34}{10}
\end{aligned}
$$

---

## Interpolation Search

This algorithm will work on **Sorted Arrays**

1. Devide array into **two parts**
2. Check **distribution center**
3. If **greater**: check **left** sub-array
4. If **smaller**: check **right** sub-array

![Interpolation Search](../../assets/interpolation_search.jpg)

Is an algorithm using these paradigms:

1. **Divide & Conquer**

```ts
const solve = (array: number[], element: number) => {
    let low = 0;
    let high = array.length - 1;

    while (low <= high) {
        let middle =
            low +
            ((high - low) * (element - array[low])) /
                (array[high] - array[low]);

        if (array[middle] === element) {
            return true;
        }

        if (array[middle] > element) {
            // Check left
            high = middle - 1;
        } else {
            // Check right
            low = middle + 1;
        }
    }

    return false;
};
```

-   Number of **compares**:

$$
\begin{aligned}
    & \texttt{Best Case}: O(1)
    \\
    & \texttt{Worst Case}: O(\log\log{n})
    \\
    & \texttt{Average Case Successful}: S(n) = \sum_{i=1}^{n} P(i).T_s(i)
    \\
    & \texttt{Average Case UnSuccessful}: U(n) = \sum_{i=1}^{n+1} P(i).T_u(i)
    \\
    \\
    & P(i): \texttt{Probability of searching item at index i}
    \\
    & T_s(i): \texttt{Number of compares needs to find successful item at index i}
    \\
    & T_u(i): \texttt{Number of compares needs to find unsuccessful item at index i}
\end{aligned}
$$

---
