# Graph

There are some common problem of graphs, can solve mostly using **Traversations**

![Orders](../../assets/graph_orders.png)

---

## Cycle Detection

The problem of **Detecting Cycles** in **Graphs**:

---

### DFS

We can use **DFS** to detect cycles in graphs

-   **Traverse** graph `DFS`: $O(n + e)$
    -   The **Back Edge** is `cycle`

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Adjacency List**: $O(n + e)$
    -   **Adjacency Matrix**: $O(n^2)$
-   **Space Complexity**: $O(n)$

---

### Disjoint Sets

We can use **Disjoint Sets** and check graph has cycle per each **AddEdge**

-   **Create** `disjoint set`
-   For every `vertex`: $O(n)$
    -   **MakeSet** vertex: $O(1)$
-   For every `edge` u, v: $O(e)$
    -   If **FindSet** `u` and `v` is equal
        -   **Cycle** will add for edge `u-v`
    -   Else
        -   **Union** `u` and `v`: $O(\alpha)$

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**:
    -   **Adjacency List**: $O(n + e\alpha) = O(n + e)$
    -   **Adjacency Matrix**: $O(n^2)$
-   **Space Complexity**: $O(n)$

---

## Topological Sort

The problem of **Sorting Vetices** in **Directed Acyclic Graphs (DAG)** in order of `number of dependencies` (`input neighbors`):

![Topological Sort](../../assets/graph_topological_sort.png)

-   **Tip**: A graph can have `many` topological sorts (**Topological Sort** is not **Unique**)

---

### DFS

We can use **DFS** to find a topological sort of a graph

-   **Traverse** graph `DFS`: $O(n + e)$
    -   **Save** `discovery` and `finish` times
-   **Sort** vertices on `finish` time `DESC`: $O(n\log{n})$

![DFS](../../assets/graph_topological_sort_dfs.png)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n\log{n} + e)$
-   **Space Complexity**: $O(n)$

---

## Articulation Points

The problem of finding **Articulated Points** or **Cut Vertices** in **UnDirected Connected Graphs**:

![Articulation Points](../../assets/graph_articulation_points.jpg)

-   **Tip**: Point is `articulation` $\iff$ `Removing` it `disconnects` the graph

---

### DFS

We can use **DFS** to find a articulation points of a graph

-   **Traverse** graph `DFS`: $O(n + e)$
    -   **Save** `back` edges
-   **Root** node of `DFS` is `articulation` $\iff$ Has `more than one child`
-   **Non-Root** node of `DFS` is `articulation` $\iff$ `No edge` from `any child` to `any parent`

![DFS](../../assets/graph_articulation_point_dfs.png)

$$
\begin{aligned}
    & \texttt{Articulation Points}: \{a, b, f\}
\end{aligned}
$$

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n + e)$
-   **Space Complexity**: $O(n)$

---

## Connected Components

The problem of finding **Largest Strongly Connected Components** in **Directed Connected Graphs**:

![Connected Components](../../assets/graph_connected_components.png)

-   **Strongly Connected Component**: Is a graph that exists at least `one path` from `any vertex` to `any vertex`
-   **Tip**: By adding an `edge` to a graph, number of components `may reduce`
-   **Tip**: Number of components in a graph is in range of $[1, n]$

---

### Kosaraju

Is a **DFS** based method to find the **Strongly Connected Components** of a graph

-   **Traverse** graph `DFS`: $O(n + e)$
    -   **Save** `discovery` and `finish` times
-   **Transpose** the graph: $O(e)$
    -   **Reverse** each `edge`
-   **Sort** vertices by `finish` time `DESC`: $O(n.\log{n})$
-   **Traverse** transposed graph `DFS` in order of `finish DESC`: $O(n + e)$
-   Each **Disconnected** graph is a `strongly connected component`

![Kosaraju](../../assets/graph_connected_components_kosaraju.png)

$$
\begin{aligned}
    & DFS(a) = \{a, b, d, f\}
    \\
    & DFS(c) = \{c, g\}
    \\
    & DFS(e) = \{e\}
\end{aligned}
$$

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n\log{n} + e)$
-   **Space Complexity**: $O(n)$

---
