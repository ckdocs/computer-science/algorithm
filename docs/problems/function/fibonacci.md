# Fibonacci Series

Fibonacci series generates the subsequent number by adding two previous numbers.

We can write a **Recurrence Relation** for **Fibonacci Series**:

$$
\begin{aligned}
    & F(n) = F(n-1) + F(n-2)
    \\
    & F(0) = F(1) = 1
\end{aligned}
$$

![Fibonacci Series](../../assets/fibonacci_series.gif)

---

## Simple Recursive

We want to find **F(n)** we start **dividing problem to get simple sub-problem F(0) and F(1)**:

We directly write a program for **Recurrence Relation**:

```ts
function fib(n: number) {
    if (n === 0 || n === 1) {
        return 1;
    }

    return fib(n - 1) + fib(n - 2);
}
```

We have the **Value Recurrence Relation**, now we write **Call Recurrence Relation**:

$$
\begin{aligned}
    & C(n) = C(n-1) + C(n-2) + 1
    \\
    & C(0) = C(1) = 1
    \\
    \\ \overset{\texttt{Recurrence Tree}}{\implies}
    & C(n) \leq 1 + 2 + 4 + \dots = 2.\sum_{i=0}^{n} 2^i = 2^n - 1
\end{aligned}
$$

-   **Time Complexity**: $O(2^n)$
-   **Space Complexity**: $O(n)$

---

## Dynamic Programming

### Recursive (Top-Down)

A **Dynamic Programming** approach implementation of fibonacci starts **from top to down**:

```ts
let cache = {};
function fib(n: number) {
    if (n === 0 || n === 1) {
        return 1;
    }

    if (!cache[n]) {
        cache[n] = fib(n - 1) + fib(n - 2);
    }

    return cache[n];
}
```

This program will recursively call until `1`:

-   **Time Complexity**: $O(2n-1)$
-   **Space Complexity**: $O(n)$

---

### Iterative (Bottom-Up)

A **Dynamic Programming** approach implementation of fibonacci starts **from bottom to top**:

```ts
function fib(n: number) {
    let f0 = 1;
    let f1 = 1;

    for (let i = 2; i <= n; i++) {
        let temp = f1;
        f1 = f0 + f1;
        f0 = temp;
    }

    if (n === 0) {
        return f0;
    }

    return f1;
}
```

This program has one loop with size **n** so:

-   **Time Complexity**: $O(n)$
-   **Space Complexity**: $O(1)$

---

## Direct Formula

An approach using **Generator Matrix**:

$$
\begin{aligned}
    &
    \begin{bmatrix}
        0 & 1
        \\
        1 & 1
    \end{bmatrix}
    \times
    \begin{bmatrix}
        f_0
        \\
        f_1
    \end{bmatrix}
    =
    \begin{bmatrix}
        f_1
        \\
        f_0 + f_1
    \end{bmatrix}
    =
    \begin{bmatrix}
        f_1
        \\
        f_2
    \end{bmatrix}
    \\
    &
    \begin{bmatrix}
        0 & 1
        \\
        1 & 1
    \end{bmatrix}^2
    \times
    \begin{bmatrix}
        f_0
        \\
        f_1
    \end{bmatrix}
    =
    \begin{bmatrix}
        f_2
        \\
        f_3
    \end{bmatrix}
    \\
    & \vdots
    \\
    &
    \begin{bmatrix}
        0 & 1
        \\
        1 & 1
    \end{bmatrix}^n
    \times
    \begin{bmatrix}
        f_0
        \\
        f_1
    \end{bmatrix}
    =
    \begin{bmatrix}
        f_n
        \\
        f_{n+1}
    \end{bmatrix}
\end{aligned}
$$

This program will multiply the generator matrix `n` times:

-   **Time Complexity**: $O(\log{n})$
-   **Space Complexity**: $O(1)$

---
