# Hashing

The problem of **Mapping a key into smaller domain** called hashing

A good hash function has these characteristics:

1. It's **monotonic** (With same probability generate addresses)
2. Depennds on entire **Key Bits**
3. Per input **always** gives **one output**
4. Simple to **compute**
5. Lowest **collision** (Two inputs gives one output)

---

## Middle Square

In this method we square the input and slice a part of it

$$
\begin{aligned}
    & h(k) = slice(k^2)
    \\
    \\
    & h(64) = slice(4096) = 09
    \\
    & h(271) = slice(73441) = 44
\end{aligned}
$$

---

## Division

In this method we find the mod of division key over number

$$
\begin{aligned}
    & h(k) = k \;mod\; x
    \\
    \\
    & h(64) = 64 \;mod\; 15 = 4
    \\
    & h(271) = 64 \;mod\; 15 = 1
\end{aligned}
$$

---

## Folding

In this method we split input with sizes and find sum of them

$$
\begin{aligned}
    & h(k) = k_1 + k_2 + \dots k_m
    \\
    \\
    & h(64) = 6 + 4 = 10
    \\
    & h(271) = 2 + 7 + 1 = 10
\end{aligned}
$$

---
