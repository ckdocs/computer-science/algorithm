# Polynomial Operations

Now we learn algorithms for basic operations on **Polynomials** or **Big Numbers**

![Orders](../../assets/polynomial_orders.png)

---

## Concepts

Numbers are similar to **Linear Polynomials**:

$$
\begin{aligned}
    & 234 = 4 \times \underbrace{10^0}_{x^0} + 3 \times \underbrace{10^1}_{x^1} + 2 \times \underbrace{10^2}_{x^2}
    \\
    & 5031 = 1 \times \underbrace{10^0}_{x^0} + 3 \times \underbrace{10^1}_{x^1} + 0 \times \underbrace{10^2}_{x^2} + 5 \times \underbrace{10^3}_{x^3}
\end{aligned}
$$

So we can define an algorithm for **multiplying polynomials**, then use it for both:

$$
\begin{aligned}
    & P(x) = a_0 + a_1.x + a_2.x^2 + \dots + a_n.x^n
    \\
    & Q(x) = b_0 + b_1.x + b_2.x^2 + \dots + b_m.x^m
    \\
    \\ \overset{?}{\implies}
    & R(x) = P(x).Q(x) = c_0 + c_1.x + c_2.x^2 + \dots + c_{n+m}.x^{n+m}
\end{aligned}
$$

---

## Multiplication

There many algorithms with different complexities for multipling polynomials:

---

### Naive

**Naive** multiplication is the classical algorithm used to multiple two polynomials or numbers with same size:

$$
\begin{aligned}
    & c_0 = a_0.b_0
    \\
    & c_1 = a_0.b_1 + a_1.b_0
    \\
    & c_2 = a_0.b_2 + a_1.b_1 + a_2.b_0
    \\
    & \dots
    \\
    & c_{n-1} = a_0.b_{n-1} + a_1.b_{n-2} + \dots + a_{n-1}.b_0
    \\
    & \dots
    \\
    & c_{2n-2} = a_{n-1}.b_{n-1}
    \\
    \\ \implies
    & c_k = \sum_{i+j=k} a_i.b_j
\end{aligned}
$$

```ts
function mul(coef1: number[], coef2: number[]) {
    let result = [];

    for (let i = 0; i < coef1.length; i++) {
        for (let j = 0; j < coef2.length; col++) {
            result[i + j] += coef1[i] * coef2[j];
        }
    }

    return result;
}
```

**Complexity**: This program has two loop with size **n** so:

-   **Time Complexity**: $O(n^2)$
-   **Space Complexity**: $O(n)$

---

### Divide & Conquer

We can write a polynomial multiplication algorithm by **Divide & Conquer** paradigm with:

1. **4 Multiplication**
2. **3 Addition**

$$
\begin{aligned}
    & P(x) = a_0 + a_1.x + a_2.x^2 + \dots + a_n.x^n
    \\ \implies
    & P(x) = a_0 + a_1.x + a_2.x^2 + \dots + a_{\frac{n}{2}-1}.x^{\frac{n}{2}-1} + a_{\frac{n}{2}}.x^{\frac{n}{2}} + \dots + a_n.x^n
    \\ \implies
    & P(x) = \underbrace{a_0 + a_1.x + a_2.x^2 + \dots + a_{\frac{n}{2}-1}.x^{\frac{n}{2}-1}}_{P_L(x)} + x^{\frac{n}{2}}.(\underbrace{a_{\frac{n}{2}} + \dots + a_n.x^{\frac{n}{2}-1}}_{P_R(x)})
    \\
    \\ \implies
    & P(x) = P_L(x) + x^{\frac{n}{2}}.P_R(x)
    \\
    & Q(x) = Q_L(x) + x^{\frac{n}{2}}.Q_R(x)
    \\
    \\ \implies
    & R(x) = P(x).Q(x) = (P_L + x^{\frac{n}{2}}.P_R) . (Q_L + x^{\frac{n}{2}}.Q_R)
    \\ \implies
    & R(x) = P_L Q_L + x^{\frac{n}{2}}.(P_L Q_R + P_R Q_L) + x^n.P_R Q_R
\end{aligned}
$$

**Complexity**: This recursive algorithm has 4 **polynomial multiples** and 3 **polynomial additions**, so we can write a **Recurrence Tree** for **Time Complexity**

$$
\begin{aligned}
    & T(n) = 4T(\frac{n}{2}) + 3n
    \\
    & T(1) = 1
    \\
    \\ \implies
    & T(n) \in \Theta(n^2)
\end{aligned}
$$

-   **Time Complexity**: $O(n^2)$
-   **Space Complexity**: $O(n)$

---

### Karatsuba

Karatsuba optimized the **Divide & Conquer** method to reduce the number of **multiplications** from 4 to 3:

1. **3 Multiplication**
2. **6 Addition/Subtraction**

![Karatsuba Multiplication](../../assets/polynomial_multiplication_karatsuba.png)

-   **Tip**: Because of increasing the number of **Summations** karatsuba algorithm is better for big **n** values

**Complexity**: This recursive algorithm has 3 **polynomial multiples** and 6 **polynomial additions/subtractions**, so we can write a **Recurrence Tree** for **Time Complexity**

$$
\begin{aligned}
    & T(n) = 3T(\frac{n}{2}) + 6n
    \\
    & T(1) = 1
    \\
    \\ \implies
    & T(n) \in \Theta(n^{\log{3}}) = \Theta(n^{1.58})
\end{aligned}
$$

-   **Time Complexity**: $O(n^{1.58})$
-   **Space Complexity**: $O(n)$

---
