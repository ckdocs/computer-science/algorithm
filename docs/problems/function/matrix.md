# Matrix Operations

Now we learn algorithms for basic operations on **Dense Matrices** and **Sparse Matrices**

![Sparse Dense Matrix](../../assets/sparse_dense_matrix.gif)

![Orders](../../assets/matrix_orders.png)

---

## Concepts

There are two types of matrices:

-   **Dense Matrix**: A ($m \times n$) array containes:
    1. **Zero** and **Non-Zero** values
       ![Dense Matrix](../../assets/dense_matrix.png)
-   **Sparse Matrix**: A ($(items + 1) \times 3$) array containes:
    1. **Matrix Size**
    2. **Non-Zero** values in **Row major**
       ![Dense To Sparse](../../assets/dense_to_sparse.png)
       ![Sparse Matrix](../../assets/sparse_matrix.png)

---

## Addition

We can add two dense matrix by adding their elements one-by-one:

-   The matrices must have the **same size**

![Dense Matrix Addition](../../assets/matrix_addition.png)

```ts
function add(mat1: number[][], mat2: number[][]) {
    let result = [[]];

    for (let row = 0; row < mat1.length; row++) {
        for (let col = 0; col < mat1[row].length; col++) {
            result[row][column] = mat1[row][column] + mat2[row][column];
        }
    }

    return result;
}
```

**Complexity**: This program has two loop with size **n** and **m** so:

-   **Time Complexity**: $O(m.n)$ = $O(n^2)$
-   **Space Complexity**: $O(1)$

---

## Subtraction

We can subtract two dense matrix by subtracting their elements one-by-one:

-   The matrices must have the **same size**

![Dense Matrix Subtraction](../../assets/matrix_subtraction.png)

```ts
function sub(mat1: number[][], mat2: number[][]) {
    let result = [[]];

    for (let row = 0; row < mat1.length; row++) {
        for (let col = 0; col < mat1[row].length; col++) {
            result[row][column] = mat1[row][column] - mat2[row][column];
        }
    }

    return result;
}
```

**Complexity**: This program has two loop with size **n** and **m** so:

-   **Time Complexity**: $O(m.n)$ = $O(n^2)$
-   **Space Complexity**: $O(1)$

---

## Multiplication

There many algorithms with different complexities for multipling matrices:

---

### Naive

**Naive** multiplication is the classical algorithm used to multiple two matrices with size $m \times n$ and $n \times p$ using **Dot Product**:

![Naive Multiplication](../../assets/matrix_multiplication_naive.gif)

```ts
function mul(mat1: number[][], mat2: number[][]) {
    let result = [[]];

    for (let row = 0; row < mat1.length; row++) {
        for (let col = 0; col < mat2[row].length; col++) {
            result[row][col] = 0;
            for (let i = 0; i < mat1[row].length; i++) {
                result[row][col] += mat1[row][i] * mat2[i][col];
            }
        }
    }

    return result;
}
```

**Complexity**: This program has three loop with size **m** and **n** and **p** so:

-   **Time Complexity**: $O(m.n.p)$ = $O(n^3)$
-   **Space Complexity**: $O(m.p)$ = $O(n^2)$

---

### Divide & Conquer

We can write a matrix multiplication algorithm by **Divide & Conquer** paradigm with:

1. **8 Multiplication**
2. **4 Addition**

![Divide & Conquer Multiplication](../../assets/matrix_multiplication_divide_conquer.png)

**Complexity**: This recursive algorithm has 8 **matrix multiples** and 4 **matrix additions**, so we can write a **Recurrence Tree** for **Time Complexity**

$$
\begin{aligned}
    & T(n) = 8T(\frac{n}{2}) + 4(\frac{n}{2})^2
    \\
    & T(1) = 1
    \\
    \\ \implies
    & T(n) \in \Theta(n^3)
\end{aligned}
$$

-   **Time Complexity**: $O(n^3)$
-   **Space Complexity**: $O(n^2)$

---

### Strassen

Strassen optimized the **Divide & Conquer** method to reduce the number of **multiplications** from 8 to 7:

1. **7 Multiplication**
2. **18 Addition/Subtraction**

![Strassen Multiplication](../../assets/matrix_multiplication_strassen.png)

-   **Tip**: Because of increasing the number of **Summations** strassen algorithm is better for big **n** values

**Complexity**: This recursive algorithm has 7 **matrix multiples** and 18 **matrix additions/subtractions**, so we can write a **Recurrence Tree** for **Time Complexity**

$$
\begin{aligned}
    & T(n) = 7T(\frac{n}{2}) + 18(\frac{n}{2})^2
    \\
    & T(1) = 1
    \\
    \\ \implies
    & T(n) \in \Theta(n^{\log{7}}) = \Theta(n^{2.81})
\end{aligned}
$$

-   **Time Complexity**: $O(n^{2.81})$
-   **Space Complexity**: $O(n^2)$

---

## Transpose

The **transpose** (**Transposition**) of a matrix is an operator which flips a matrix over its diagonal; that is, it switches the row and column indices of the matrix $A$ by producing another matrix, often denoted by $A^T$

![Matrix Transpose](../../assets/matrix_transpose.gif)

```ts
function transpose(mat: number[][]) {
    let result = [[]];

    for (let row = 0; row < mat1.length; row++) {
        for (let col = 0; col < mat2[row].length; col++) {
            result[col][row] = mat[row][col];
        }
    }

    return result;
}
```

**Complexity**: This program has two loop with size **m** and **n** so:

-   **Time Complexity**: $O(m.n)$ = $O(n^2)$
-   **Space Complexity**: $O(m.n)$ = $O(n^2)$

---
