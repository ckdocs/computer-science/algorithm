# Arithmetic

![Orders](../../assets/arithmetic_orders.png)

---

## Summation

The problem of **Summation** of numbers `1 to n`:

$$
\begin{aligned}
    & sum: n \mapsto 1 + 2 + \dots + n
\end{aligned}
$$

---

### Simple Recursive

First we must find the **Recurrence Relation** equivalent to the **Problem**:

$$
\begin{aligned}
    & sum(n) = \underbrace{1 + 2 + \dots + (n-1)}_{sum(n-1)} + n
    \\ \implies
    & sum(n) = sum(n-1) + n
\end{aligned}
$$

```ts
function sum(n: number) {
    if (n === 1) {
        return 1;
    }

    return sum(n - 1) + n;
}
```

---

### Dynamic Programming

We can write the **Bottom-Up** version of **Simple Recursive** using **Dynamic Programming**:

```ts
function sum(n: number) {
    let sum = 0;
    for (let i = 1; i <= n; i++) {
        sum += i;
    }

    return sum;
}
```

---

### Direct Formula

By solving the **Recurrence Relation** of problem we can find the direct formula:

$$
\begin{aligned}
    & sum (n) = sum(n-1) + n
    \\ \implies
    & sum(n) = \frac{n.(n+1)}{2}
\end{aligned}
$$

```ts
function sum(n: number) {
    return (n * (n + 1)) / 2;
}
```

---

## Factorial

The problem of **Multiplication** of numbers `1 to n`

$$
\begin{aligned}
    & fact: n \mapsto 1 \times 2 \times \dots \times n
\end{aligned}
$$

---

### Simple Recursive

First we must find the **Recurrence Relation** equivalent to the **Problem**:

$$
\begin{aligned}
    & fact(n) = \underbrace{1 \times 2 \times \dots \times (n-1)}_{fact(n-1)} \times n
    \\ \implies
    & fact(n) = fact(n-1) \times n
\end{aligned}
$$

```ts
function fact(n: number) {
    if (n === 1) {
        return 1;
    }

    return fact(n - 1) * n;
}
```

---

### Dynamic Programming

We can write the **Bottom-Up** version of **Simple Recursive** using **Dynamic Programming**:

```ts
function fact(n: number) {
    let prod = 1;
    for (let i = 1; i <= n; i++) {
        prod *= i;
    }

    return prod;
}
```

---

## Division

The problem of **Dividing** two numbers and finding **Quotient** (`div`)

$$
\begin{aligned}
    & div: k, n \mapsto \frac{n}{k}
    \\
    \\
    & \frac{n}{k} \implies n = \underbrace{k + k + \dots + k}_{q} + r
    \\ \implies
    & n = k.q + r
    \\
    \\
    & \textbf{n}: \texttt{Dividend}
    \\
    & \textbf{k}: \texttt{Divisor}
    \\
    & \textbf{q}: \texttt{Quotient}
    \\
    & \textbf{r}: \texttt{Remainder}
\end{aligned}
$$

---

### Simple Recursive

We use **Euclidean** formula for finding `quotient`:

$$
\begin{aligned}
    & div(k, n) =
    \begin{cases}
        div(k, n - k) + 1 & n \geq k
        \\
        0 & n \lt k
    \end{cases}
\end{aligned}
$$

```ts
function div(k: number, n: number) {
    if (n < k) {
        return 0;
    }

    return div(k, n - k) + 1;
}
```

---

### Dynamic Programming

We can write the **Bottom-Up** version of **Simple Recursive** using **Dynamic Programming**:

```ts
function div(k: number, n: number) {
    let div = 0;
    while (n >= k) {
        n -= k;
        div++;
    }

    return div;
}
```

---

## Remainder

The problem of **Dividing** two numbers and finding **Remainder** (`mod`, `rem`)

$$
\begin{aligned}
    & rem: k, n \mapsto n \;\%\; k
\end{aligned}
$$

---

### Simple Recursive

We use **Euclidean** formula for finding `remainder`:

$$
\begin{aligned}
    & mod(k, n) =
    \begin{cases}
        mod(k, n - k) & n \geq k
        \\
        n & n \lt k
    \end{cases}
\end{aligned}
$$

```ts
function mod(k: number, n: number) {
    if (n < k) {
        return n;
    }

    return mod(k, n - k);
}
```

---

### Dynamic Programming

We can write the **Bottom-Up** version of **Simple Recursive** using **Dynamic Programming**:

```ts
function mod(k: number, n: number) {
    while (n >= k) {
        n -= k;
    }

    return n;
}
```

---

## Exponent

The problem of **Exponenting** a `number` in the `power` of `another number`

$$
\begin{aligned}
    & pow: k, n \mapsto k^n
\end{aligned}
$$

---

### Simple Recursive

First we must find the **Recurrence Relation** equivalent to the **Problem**:

$$
\begin{aligned}
    & pow(k, n) = \underbrace{k \times k \times \dots \times k}_{pow(k, n-1)} \times k
    \\ \implies
    & pow(k, n) = pow(k, n-1) \times k
\end{aligned}
$$

```ts
function pow(k: number, n: number) {
    if (n === 0) {
        return 1;
    }

    return pow(k, n - 1) * k;
}
```

---

### Dynamic Programming

We can write the **Bottom-Up** version of **Simple Recursive** using **Dynamic Programming**:

```ts
function pow(k: number, n: number) {
    let prod = 1;
    for (let i = 1; i <= n; i++) {
        prod *= k;
    }

    return prod;
}
```

---

### Optimized Recursive

We cannot solve the **Recurrence Relation** above, but we can write a better relation with better complexity using **Divide & Conquer** method:

$$
\begin{aligned}
    & k^n = k^{\frac{n}{2}} \times k^{\frac{n}{2}} = (k^{\frac{n}{2}})^2
    \\
    \\ \implies
    & pow(k, n) =
    \begin{cases}
        pow^2(k, \frac{n}{2}) & \texttt{n is even}
        \\
        \\
        k \times pow^2(k, \lfloor \frac{n}{2} \rfloor) & \texttt{n is odd}
    \end{cases}
\end{aligned}
$$

```ts
function pow(k: number, n: number) {
    if (n === 0) {
        return 1;
    }

    if (n % 2 === 0) {
        const pow = pow(k, n / 2);
        return pow * pow;
    } else {
        const pow = pow(k, n / 2);
        return k * pow * pow;
    }
}
```

---

## Combination

The problem of **Combinating** or choosing `k` items from `n` items

$$
\begin{aligned}
    & comb: k, n \mapsto C(n, k) = C_k^n = {n \choose k}
\end{aligned}
$$

---

### Simple Recursive

We use **Pascal** formula for finding `combination`:

$$
\begin{aligned}
    & {n \choose k} = {n-1 \choose k} + {n-1 \choose k-1} = \frac{n}{k} {n-1 \choose k-1}
    \\
    & {n \choose 0} = {n \choose n} = 1
    \\
    \\ \implies
    & comb(n, k) =
    \begin{cases}
        comb(n - 1, k) + comb(n - 1, k - 1) & Otherwise
        \\
        1 & n = k \;or\; k = 0
    \end{cases}
\end{aligned}
$$

```ts
function comb(k: number, n: number) {
    if (n === k || k === 0) {
        return 1;
    }

    return comb(k, n - 1) + comb(k - 1, n - 1);
}
```

---

### Dynamic Programming

We can write the **Bottom-Up** version of **Simple Recursive** using **Dynamic Programming**:

```ts
function comb(k: number, n: number) {
    let comb = [];

    for (let i = 0; i <= n; i++) {
        for (let j = i; j >= 0; j--) {
            if (j == 0 || j == i) {
                comb[j] = 1;
            } else {
                comb[j] = comb[j] + comb[j - 1];
            }
        }
    }

    return comb[k];
}
```

---

### Direct Formula

$$
\begin{aligned}
    & {n \choose k} = \frac{n!}{(n - k)! k!}
\end{aligned}
$$

```ts
function comb(k: number, n: number) {
    let fn = factorial(n);
    let fk = factorial(k);
    let fnk = factorial(n - k);

    return fn / (fk * fnk);
}
```

-   **Tip**: The problems of this algorithm is **storing the big numbers** from factorial, for example ${100 \choose 99}$ is small number but **100!** is a large number:

$$
\begin{aligned}
    & {100 \choose 99} = \frac{100!}{(100 - 99)! \times 99!} = \frac{100!}{99!} = 100
\end{aligned}
$$

---

## GCD

The problem of finding **Greatest** positive integer that can divide both **a** and **b**, it may comes with other names:

1. Greatest Common Factor (**GCF**)
2. Greatest Common Divisor (**GCD**)
3. Highest Common Factor (**HCF**)
4. Highest Common Divisor (**HCD**)

![GCD LCM](../../assets/gcd_lcm.png)

-   **GCD**: The **Intersection** of `prime factors` of two numbers
-   **LCM**: The **Union** of `prime factors` of two numbers

$$
\begin{aligned}
    & gcd: m, n \mapsto GCD(m, n)
\end{aligned}
$$

---

### Subtraction

We recursively **subtract** the **smaller** number from the **larger**

```ts
function gcd(n: number, m: number) {
    if (n === m) {
        return n;
    } else if (n > m) {
        return gcd(m - n, n);
    } else {
        return gcd(m, n - m);
    }
}
```

---

### Division

We recursively **find reminder of division** the **smaller** number from the **larger**

-   In each call the `n` will divide by `2`

![GCD Division](../../assets/gcd_division.png)

$$
\begin{aligned}
    & gcd(n, m) =
    \begin{cases}
        n & m = 0
        \\
        gcd(m, n \;mod\; m) & m \neq 0
    \end{cases}
\end{aligned}
$$

```ts
function gcd(n: number, m: number) {
    if (m === 0) {
        return n;
    }

    return gcd(m, n % m);
}
```

---

## LCM

The problem of finding **Smallest** positive integer that is divisible by both **a** and **b**, it may comes with other names:

1. Least Common Multiple (**LCM**)

$$
\begin{aligned}
    & lcm: m, n \mapsto LCM(m, n)
\end{aligned}
$$

---

### Using GCD

We can find **LCM** using **GCD** by this formula

$$
\begin{aligned}
    lcm(m, n) = \frac{m \times n}{gcd(m, n)}
\end{aligned}
$$

---
