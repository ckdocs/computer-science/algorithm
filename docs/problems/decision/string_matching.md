# String Matching

The problem of **Checking** a **Parent** exists in a **Text** as **SubString** or **SubSequence**

$$
\begin{aligned}
    & Text = t_1 t_2 \dots t_n
    \\
    & Pattern = p_1 p_2 \dots p_m
    \\
    & m \lt n
\end{aligned}
$$

---

## IsSubsequence

-   **Set** `t` on **Text**
-   **Set** `p` on **Pattern**
-   **Check** equality of characters
-   **If** equal:
    -   **Move** forward the `t`, `p`
-   **Else**:
    -   **Move** forward the `t`

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n)$

---

## IsSubstring

There are many methods for substring checking

---

### Naive

-   **Set** `t` on **Text**
-   **Set** `p` on **Pattern**
-   **Check** equality of characters
-   **If** equal:
    -   **Move** forward the `t`, `p`
-   **Else**:
    -   **Move** backward the `t` by `p`
    -   **Set** `p` to `0`

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(nm)$

---

### Automata

-   **Creater** a `DFA` of pattern: $O(m|\Sigma|)$
-   **Accept** the `text` using `DFA`: $O(n)$

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(m|\Sigma| + n)$

---

### KMP

Knuth-Morris-Pratt

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(m + n)$

---

### Rabin-Karp

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(m + mn)$

---
