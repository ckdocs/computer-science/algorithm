# Matrix Chain Multiplication

Given a **Sequence of Matrices**, find the most `efficient` way to multiply these matrices together, with the `lowest` **Number Multiplications**

![Matrix Chain](../../assets/matrix_chain.gif)

We have **Matrices Sizures** of each key search, we want to reduce the total number of integer multiplications

-   **Tip**: The `greedy solution` for this problem is to **Reduce** the `number of multiplications` of **Larger Dimensions**

---

## Brute Force

-   **Foreach** multiplication state: $O(4^n)$
    -   **Find** number of multiples
-   **Find** minimum cost

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(C_n) \simeq O(4^n)$

---

## Dynamic Programming

We want to find the **Minimum Number of Multiples** of multiplying matrices `i to j`:

$$
\begin{aligned}
    & F(i, j) = min
    \left.\begin{cases}
        F(i, i) + F(i + 1, j) + (r_i*r_k*r_j) & break point (k) = i
        \\
        F(i, i + 1) + F(i + 2, j) + (r_i*r_k*r_j) & break point (k) = i + 1
        \\
        \vdots
        \\
        F(i, j - 1) + F(j, j) + (r_i*r_k*r_j) & break point (k) = j - 1
    \end{cases}\right\}
\end{aligned}
$$

-   **Select** 1 as `break point`
    -   **Recursive** call for `left matrices`
    -   **Recursive** call for `right matrices`
    -   **Add** number of multiples of `left` and `right` multiplications
-   **Select** 2 as `break point`
    -   **Recursive** call for `left matrices`
    -   **Recursive** call for `right matrices`
    -   **Add** number of multiples of `left` and `right` multiplications
-   ...
-   **Select** n as `break point`
    -   **Recursive** call for `left matrices`
    -   **Recursive** call for `right matrices`
    -   **Add** number of multiples of `left` and `right` multiplications
-   **Minimum** of `1 as break point` and `2 as break point` and ... and `n as break point`

![Dynamic Programming](../../assets/matrix_chain_dynamic_programming.png)

```ts
const matrices = [
    // ...
];

@memoize()
const solve = (i: number, j: number) => {
    if (i < j - 1) {
        return 0;
    }

    let multiples = [];
    for (let k = i; k < j; k++) {
        costs.push(
            // Minimum for left matrices
            solve(i, k) +
                // Minimum for right matrices
                solve(k + 1, j) +
                // Number of multiples for result
                matrices[i].rows * matrices[k].cols * matrices[j].cols
        );
    }

    // Minimum number of multiples
    return Math.min(multiples);
};
```

**Complexity**: Complexity of this algorithm is:

$$
\begin{aligned}
    & T(n) = \sum_{L=2}^{n} \sum_{i=1}^{n-L+1} \sum_{k=i}^{j} 1
    \\ \implies
    & T(n) = \frac{n^3 - n}{6}
\end{aligned}
$$

-   **Time Complexity**: $O(n^3)$

---
