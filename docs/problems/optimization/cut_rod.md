# Cut Rod

We have a **Rod** with length of `n`, each size of rod has a `profit`, we want to **Cut** our rod in order to making the `most profit`

![Cut Rod](../../assets/cut_rod.png)

---

## Brute Force

Each **Point** on the rod can **Cut** or **Release**, we have **n-1** points on a rod of length **n**

-   **Foreach** cut point permute: $O(2^{n-1)$
    -   **Find** the profit

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(2^{n-1})$

---

## Dynamic Programming

We want to find the **Maximum Profit** by cutting a rod of length `i`:

$$
\begin{aligned}
    & F(i) = max
    \left.\begin{cases}
        P_1 + F(i-1) & cut point (k) = 1
        \\
        P_2 + F(i-2) & cut point (k) = 2
        \\
        \vdots
        \\
        P_i + F(i-i) & cut point (k) = i
    \end{cases}\right\}
\end{aligned}
$$

-   **Select** 1 as `cut point` from `end`
    -   **Add** profit for cutted end of rod `1`
    -   **Recursive** call for begining of rod `i-1`
-   **Select** 2 as `cut point` from `end`
    -   **Add** profit for cutted end of rod `2`
    -   **Recursive** call for begining of rod `i-2`
-   ...
-   **Select** i as `cut point` from `end`
    -   **Add** profit for cutted end of rod `i`
    -   **Recursive** call for begining of rod `i-i`
-   **Maximum** of `1 as cut point` and `2 as cut point` and ... and `i as cut point`

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n^2)$

---
