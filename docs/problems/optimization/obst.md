# Optimal BST

**Optimal BST** or **Weight-Balanced BST** is a binary search tree which provides the `smallest` possible `search time`

![Optimal BST](../../assets/optimal_bst.png)

We have **Sorted Keys** and **Probabilities** of each key search, we want to reduce the total searching cost:

$$
\begin{aligned}
    & \texttt{Expected Cost}: \sum_{i=1}^{n} cost(k_i).p_i
    \\ =
    & \sum_{i=1}^{n} (depth(k_i) + 1).p_i
    \\ =
    & \sum_{i=1}^{n} depth(k_i).p_i + \sum_{i=1}^{n} p_i
    \\ =
    & \sum_{i=1}^{n} depth(k_i).p_i + 1
\end{aligned}
$$

---

## Brute Force

-   **Foreach** BST topologies: $O(4^n)$
    -   **Find** expected cost
-   **Find** minimum cost

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(C_n) \simeq O(4^n)$

---

## Dynamic Programming

We want to find the **Minimum Expected Cost** of a possible BST with keys `i to j`:

$$
\begin{aligned}
    & F(i, j) = min
    \left.\begin{cases}
        F(i, i - 1) + F(i + 1, j) & root = i
        \\
        F(i, i) + F(i + 2, j) & root = i + 1
        \\
        \vdots
        \\
        F(i, j - 1) + F(j + 1, j) & root = j
    \end{cases}\right\}
    + (p_i + p_{i+1} + \dots + p_{j})
\end{aligned}
$$

-   **Select** 1-th key as `root`
    -   **Recursive** call for `left sub-tree`
    -   **Recursive** call for `right sub-tree`
-   **Select** 2-th key as `root`
    -   **Recursive** call for `left sub-tree`
    -   **Recursive** call for `right sub-tree`
-   ...
-   **Select** n-th key as `root`
    -   **Recursive** call for `left sub-tree`
    -   **Recursive** call for `right sub-tree`
-   **Minimum** of `1-th as root` and `2-th as root` and ... and `n-th as root`
-   **Add** expected cost of `root` as layer

![Dynamic Programming](../../assets/optimal_bst_dynamic_programming.png)

```ts
const keys = [
    // ...
];

@memoize()
const solve = (i: number, j: number) => {
    if (i < j - 1) {
        return 0;
    }

    let costs = [];
    for (let t = i; t <= j; t++) {
        costs.push(solve(i, t - 1) + solve(t + 1, j));
    }

    // Minimum cost of root + i to j probabilities
    return (
        Math.min(costs) +
        keys.slice(i, j).reduce((acc, key) => acc + key.probability, 0)
    );
};
```

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(n^3)$

---
