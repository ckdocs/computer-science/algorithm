# Longest Common Subsequence

The problem of **Finding** longest common subsequence between two string

$$
\begin{aligned}
    & X = x_1 x_2 \dots x_n
    \\
    & Y = y_1 y_2 \dots y_m
\end{aligned}
$$

![LCS](../../assets/lcs.jpg)

---

## Concepts

### Substring

Is a **Contiguous** `sequence` of elements within an `string`

$$
\begin{aligned}
    & \texttt{Text}: \text{abababcccccba}
    \\
    & \texttt{SubString}: \text{abab---------} = \text{abab}
\end{aligned}
$$

---

### Subsequence

Is a **Sequence** that can be `derived` from another sequence by **Deleting** some or no elements `without changing the order` of the **Remaining** elements

$$
\begin{aligned}
    & \texttt{Text}: \text{abababcccccba}
    \\
    & \texttt{SubSequence}: \text{ab----------a} = \text{aba}
\end{aligned}
$$

-   **Tip**: An string of length $n$ has $2^n$ subsequences by **Selecting** or **Rejecting** each character

---

## Brute Force

-   **Foreach** subsequence of **X**: $O(2^n)$
    -   **Check** is subsequence of **Y**: $O(m)$

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(m.2^n)$

---

## Dynamic Programming

We want to find the **Longest Common Subsequence** length from index `o to i` in **X** and `0 to j` in **Y**:

$$
\begin{aligned}
    & F(i, j) = min
    \left.\begin{cases}
        F(i-1, i-1) & x_i = y_j
        \\
        max
        \left.\begin{cases}
            F(i-1, j)
            \\
            F(i, j-1)
        \end{cases}\right\} & x_i \neq y_j
        \\
        0 & i=0 \;||\; j=0
    \end{cases}\right\}
\end{aligned}
$$

-   **Check** that **X[i] == Y[j]**
-   **If** they are `equal`:
    -   **Add** last characters to common subsequence
-   **Else**:
    -   **Find** maximum of **X[i-1]** or **Y[j-1]**

![Dynamic Programming](../../assets/lcs_dynamic_programming.gif)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(mn)$
-   **Time Complexity Of Printing LCS**: $O(m+n)$
-   **Space Complexity Of Printing LCS**: $\Theta(mn)$
-   **Optimized Space Complexity Of Printing LCS**: $\Theta(min\{m,n\})$

---
