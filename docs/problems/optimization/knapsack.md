# Knapsack

Given the `weights` and `profits` of **N items**, we are asked to put these items in a knapsack that has a `capacity` **C**. The goal is to get the `maximum profit` from the items in the knapsack. Each `item` can only be `selected once`, as we don’t have multiple quantities of any item.

$$
\begin{aligned}
    & \texttt{Knapsack capacity}: C
    \\
    & \texttt{i-th Item weight}: w_i
    \\
    & \texttt{i-th Item profit}: p_i
\end{aligned}
$$

![Knapsack](../../assets/knapsack.png)

---

## Fractional Knapsack

In this version of knapsack problem, we can select any **Fraction** of any item

---

### Greedy

-   **Compute** the **priority** as $\frac{p_i}{w_i}$ for all items: $O(n)$
-   **Sort** items by `priority DESC`: $O(n.\log{n})$
-   **Select** `maximum of first, second, ...` while knapsack `is not full`: $O(n)$

```ts
const solve = (
    capacity: number,
    items: { profit: number; weight: number }[]
) => {
    // Compute priority: O(n)
    items = items.map((item) => ({
        ...item,
        priority: item.profit / item.weight,
    }));

    // Sort items: O(nlogn)
    items = items.sort((item) => item.priority);

    // Select items: O(n)
    let maxProfit = 0;
    while (capacity > 0) {
        const item = items.shift();

        if (item.weight >= capacity) {
            maxProfit += item.profit;
            capacity -= item.capacity;
        } else {
            maxProfit += (capacity / item.weight) * item.profit;
            capacity = 0;
        }
    }

    return maxProfit;
};
```

**Complexity**: Complexity of this algorithm is summation of steps complexity:

-   **Time Complexity**: $O(n.\log{n} + 2n) = O(n.\log{n})$

---

### Divide & Conquer

-   **Compute** the **priority** as $\frac{p_i}{w_i}$ for all items: $O(n)$
-   **Select** the `median` of priorities index: $O(n)$
-   **Partition** items by `median index`: $O(n)$
-   If **Right** `weights sum` is `less than capacity`:
    -   **Add** them to knapsack
    -   **Recursive** call for left: $T(\frac{n}{2})$
-   Else:
    -   **Recursive** call for right: $T(\frac{n}{2})$

![Divide & Conquer](../../assets/fractional_knapsack_divide_conquer.png)

```ts
const solve = (
    capacity: number,
    items: { profit: number; weight: number },
    left: number,
    right: number
) => {
    // Compute priority: O(n)
    items = items.map((item) => ({
        ...item,
        priority: item.profit / item.weight,
    }));

    // Select median index: O(n)
    let medianIndex = select(items, items.length / 2);

    // Parition by median: O(n)
    medianIndex = partition(items, medianIndex);

    // Find weight sum of greaters: O(n)
    const G = ...;

    // Recursive call for left or right: T(n/2)
    if (G < capacity) {
        return solve(capacity - G, items, left, medianIndex);
    } else {
        return solve(capacity, items, medianIndex, right);
    }
};
```

**Complexity**: Complexity of this algorithm is:

$$
\begin{aligned}
    & T(n) = T(\frac{n}{2}) + \Theta(n)
    \\
    \\ \implies
    & T(n) \in \Theta(n)
\end{aligned}
$$

-   **Time Complexity**: $O(n)$

---

## 0-1 Knapsack

In this version of knapsack problem, we can only select **Whole** of an item

---

### Brute Force

-   **Permute** all **items**: $O(2^n)$
-   **Filter** the **items set** with weight sum **larger than capacity**: $O(2^n)$
-   **Select** the **items set** with maximum profit: $O(2^n)$

![Brute Force](../../assets/01_knapsack_bruteforce.svg)

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(2^n)$

---

### Dynamic Programming

We want to find the **Maximum Profit** of selecting from items `1 to n` items with capacity of `c`:

| **Item**   | A   | B   | C   | D   |
| ---------- | --- | --- | --- | --- |
| **Profit** | 24  | 18  | 18  | 10  |
| **Weight** | 24  | 10  | 10  | 7   |

$$
\begin{aligned}
    & F: n, c \mapsto \texttt{Maximum possible profit}
    \\
    & F(1, 25): \{A\} = 24
    \\
    & F(2, 25): \{A\} = 24
    \\
    & F(3, 25): \{B, C\} = 24
    \\
    \\
    & F(n, c) = max
    \left.\begin{cases}
        P_n + F(n-1, c - W_n)
        \\
        F(n-1, c)
    \end{cases}\right\}
\end{aligned}
$$

-   **Select** `n-th` item if possible
    -   **Recursive** call for `n-1`
-   **Reject** `n-th` item and
    -   **Recursive** call for `n-1`
-   **Maximum** of `with n-th` and `without n-th`

![Dynamic Programming](../../assets/01_knapsack_dynamic_programming.png)

```ts
const items = [
    // ...
];

@memoize()
const solve = (capacity: number, n: number) => {
    if (n < 0) {
        return 0;
    }

    // Check n-th is selectable
    if (items[n].weight > capacity) {
        return solve(capacity, n - 1);
    }

    // Maximum profit with n-th and without n-th
    return Math.max(
        // With n-th
        items[n].profit + solve(capacity - items[n].weight, n - 1),
        // Without n-th
        solve(capacity, n - 1)
    );
};
```

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(nc) \neq O(n^2)$

---
