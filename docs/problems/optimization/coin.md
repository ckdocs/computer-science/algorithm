# Coin Change

Problem of **Spliting** some amount with **Smllest** number of **Coins**

![Coin Change](../../assets/coin_change.png)

$$
\begin{aligned}
    & \texttt{Amount}: 36
    \\
    & \texttt{Coins}: 20, 10, 5, 1
    \\
    \\ \implies
    & \textbf{36} = 20 + 10 + 5 + 1
\end{aligned}
$$

---

## Greedy

There is a greedy algorithm for this problem:

-   **Sort** coins `DESC`
-   **Select** greatest coin:
    -   If it's **Less** than amount:
        -   **Reduce** the coin from amount
        -   **Goto** step 2
    -   Else
        -   **Remove** coin
        -   **Goto** step 2

```ts
const solver = (n: number) => {
    let result = [];
    while (n > 0) {
        if (n > 20) {
            // Select 20
            result.push(20);
            n -= 20;
        } else if (n > 10) {
            // Select 10
            result.push(10);
            n -= 10;
        } else if (n > 5) {
            // Select 5
            result.push(5);
            n -= 5;
        } else if (n > 1) {
            // Select 1
            result.push(1);
            n -= 1;
        }
    }

    return result;
};
```

-   **Tip**: Greedy method for this problem does `not always` give the `optimal answer`:

$$
\begin{aligned}
    & \texttt{Amount}: 30
    \\
    & \texttt{Coins}: 25, 10, 1
    \\
    \\ \implies
    & \textbf{Greedy(30)} = 25 + 1 + 1 + 1 + 1 + 1
    \\ \implies
    & \textbf{Optimal(30)} = 10 + 10 + 10
\end{aligned}
$$

-   **Tip**: Greedy method does `not give optimal` answers in certain `situation`:

$$
\begin{aligned}
    & \texttt{Coins}: a, b, c
    \\
    \\
    & a + c \lt 2b \implies \texttt{Greedy cannot solve optimal}
\end{aligned}
$$

---

## Dynamic Programming

It's like the **0-1 Knapsack** which items are our **Coins** with **Infinite** count, and weights are `coin values` and profits are `1`, and we want to **Minimize** the profit

| **Coins**  | A   | B   | C   | D   |
| ---------- | --- | --- | --- | --- |
| **Profit** | 1   | 1   | 1   | 1   |
| **Weight** | 1   | 5   | 10  | 25  |

$$
\begin{aligned}
    & F: n, c \mapsto \texttt{Minimum possible profit}
    \\
    & F(1, 25): \{A, A, \dots, A\} = 25
    \\
    & F(2, 25): \{B, B, B, B, B\} = 5
    \\
    & F(3, 25): \{C, C, B\} = 3
    \\
    \\
    & F(n, c) = min
    \left.\begin{cases}
        1 + F(n, c - W_n)
        \\
        F(n-1, c)
    \end{cases}\right\}
\end{aligned}
$$

-   **Select** `n-th` item if possible
    -   **Recursive** call for `n`
        -   We **Can** select it `again` (**Infinite** count)
-   **Reject** `n-th` item and
    -   **Recursive** call for `n-1`
-   **Minimum** of `with n-th` and `without n-th`

![Dynamic Programming](../../assets/coin_change_dynamic_programming.jpg)

```ts
const coins = [
    // ...
];

@memoize()
const solve = (amount: number, n: number) => {
    if (n < 0) {
        return 0;
    }

    // Check n-th is selectable
    if (coins[n].price > amount) {
        return solve(amount, n - 1);
    }

    // Minimum profit with n-th and without n-th
    return Math.min(
        // With n-th (Can select again)
        1 + solve(amount - coins[n].price, n),
        // Without n-th
        solve(amount, n - 1)
    );
};
```

**Complexity**: Complexity of this algorithm is:

-   **Time Complexity**: $O(nc) \neq O(n^2)$

---
