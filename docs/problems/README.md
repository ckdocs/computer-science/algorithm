# Problems

We can categorize problems using two parameters:

-   **Complexity**
-   **Paradigm**

---

## Complexity

-   **P**: Polynomial
    -   They can **Solve** in `polynomial` time
-   **NP**: Non-Deterministic Polynomial
    -   They can **Accept** in `polynomial` time
    -   **Decision** problem
-   **Co-NP**: Complement NP
    -   They can **Reject** in `polynomial` time
    -   **Decision** problem
-   **NP-complete**
    -   Hardest NP problems
-   **NP-hard**
    -   They can **Accept** or **Reject** in `polynomial` or `exponential` time

![Complexities](../assets/problems_complexities.png)

$$
\begin{aligned}
    & P \subset NP \cap Co-NP
    \\
    & NP-Complete = NP \cap NP-Hard
    \\
    \\
    & \forall A \in NP \;:\; \overline{A} \in Co-NP
    \\
    & \forall A \in Co-NP \;:\; \overline{A} \in NP
    \\
    & \forall A \in P \;:\; \overline{A} \in P
\end{aligned}
$$

---

### P = NP

By finding a **Polynomial** solution for any **NP-Complete** or **NP-Hard** problems, it is proove that **P=NP**

![P NP](../assets/problems_p_np.png)

---

### Reduction

Is an `algorithm` for **Transforming** one `problem` into `another` problem. A sufficiently efficient reduction from one problem to another may be used to show that the second problem is at least as **Difficult** as the `first`.

$$
\begin{aligned}
    & A \le_P B
\end{aligned}
$$

-   Problem **A** reduced to problem **B** in **Polynomial** complexity
-   If **B** can be solved in $O(Polynomial)$, problem **A** can be solved in $O(Polynomial)$
    -   $Easy(B) \implies Easy(A)$
-   If **A** cannot be solved in $O(Polynomial)$, problem **B** cannot be solved in $O(Polynomial)$
    -   $Hard(A) \implies Hard(B)$

---

### NP-Complete

There are many famous **NP-Complete** problems:

1. **SAT**: Is **X** satisfies this boolean expression?
2. **3-CNF SAT**: Is **X** satisfies this boolean expression?
    - ![3-CNF SAT](../assets/problems_3cnf_sat.png)
3. **K-Clique**: Is **X** a complete sub-graph with **K vertices** in this graph?
    - ![K-Clique](../assets/problems_kclique.png)
4. **Vertex Cover**: Is **X** set, a vertex cover of this graph vertices?
    - ![Vertex Cover](../assets/problems_vertex_cover.png)
5. **Hamiltonian Cycle**: Is **X** a hamiltonian cycle of this graph?
6. **Shortest Path**: Is there a path from **X** to **Y** with a minimum **K** length?
7. **Traveling Salesman**: Optimal **Hamiltonian Cycle** in a **Weighted Graph**
    - ![Traveling Salesman](../assets/problems_traveling_salesman.png)
8. **Partition**: Split a set into `two subsets` with **Equal Sum**
9. **Sum Of Subsets**
10. **0-1 Knapsack**
11. **Coin Change**
12. **Maximum Cut**

![NP-Complete](../assets/problems_np_complete.jpg)

---

### New Problems

When we face with a `new unknown` problem **X**, we can prove it's in **NP-Complete** group by:

-   **Prove**: $X \in NP$
-   **Find** problem **Y** in **NP-Complete** group
-   **Prove**: $X \le_P Y$

---

## Paradigm

-   **Decision** problems
-   **Search** problems
-   **Counting** problems
-   **Optimization** problems
-   **Function** problems

---
