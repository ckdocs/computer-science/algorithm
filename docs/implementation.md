# Implementation

Algorithm is a **Step-by-Step Procedure**, we can implement it in **Computational Models**:

1. **Turing Machine**: (`Machine`)
2. **Lambda Calculus**: (`Function`)
3. **Word RAM**: (`Procedure`)

---

## Turing Machine (Imperative)

Is a **mathematical model** of computation that defines an **abstract machine**, which manipulates **symbols** on a strip of **tape** according to a **table of rules**

---

## Lambda Calculus (Declarative)

Is a **formal system** in mathematical logic for expressing computation based on **function abstraction** and application using **variable binding** and **substitution**

---

## Word RAM (Computer)

Is the improved version of **Turing Machine** with more **Instructions** and more **Word Size**:

-   **Word Size**: 32-bit
-   **Instruction Set**: Hardware instructions for **32-bit** words in **O(1)**
    -   **Arithmetic**: $+, -, \times, \div, \%$
    -   **Relational**: $=, \neq, \gt, \lt, \geq, \leq$
    -   **Logical**: $\&\&, ||, !$
    -   **Memory**: $Load, Store$
    -   **Jump**: $Jump, JumpIF$

Our **Personal-Computers** is an implementation of this model, and **Assembly Language** is the language of defining algorithms for this model

---

### Visualize

We can visualize our algorithm in **Structured** and **Un-Structured** format, any algorithm must observe the following:

1. **Start**, **Stop** steps
2. **Input** params after start
3. **Output** params before end

Imagine we want to add two number, we can use this algorithm:

```txt
Step 1 − START
Step 2 − declare three integers a, b & c
Step 3 − define values of a & b
Step 4 − add values of a & b
Step 5 − store output of step 4 to c
Step 6 − print c
Step 7 − STOP
```

Now we can structurize this algorithm in two formats:

1. **Pseudo-Code**
2. **Flow-Chart**

---

#### Pseudo-Code

A text without any ambiguity, contains algorithm **steps** also with `condition`, `loop` statements, it's like a **Programming Language** but it's not technically valid:

```js
sum (a, b) => {
    let c = a + b;
    return c;
}
```

---

#### Flow-Chart

A graphically representation of our algorithm

Flowchart diagrams, contains a basic symbols for:

1. **Start**, **Stop**
2. **Input**, **Output**
3. **Decision**
4. **Process**

![Flowchart](./assets/flowchart.jpg)

---

-   **Example**: Add two numbers

![Flowchart Add Two Numbers](./assets/flowchart_add_two_numbers.png)

---
